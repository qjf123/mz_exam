/*
Navicat MySQL Data Transfer

Source Server         : a
Source Server Version : 50727
Source Host           : 127.0.0.1:3306
Source Database       : mz_exam

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2021-10-31 10:30:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for b_student
-- ----------------------------
DROP TABLE IF EXISTS `b_student`;
CREATE TABLE `b_student` (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `stuName` varchar(255) NOT NULL,
                             `stuId` varchar(255) NOT NULL,
                             `stuMajor` varchar(255) DEFAULT NULL,
                             `stuGrade` varchar(255) DEFAULT NULL,
                             `stuPassword` varchar(255) NOT NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of b_student
-- ----------------------------
INSERT INTO `b_student` VALUES ('1', '11', '11111', '11', '11', '123');
INSERT INTO `b_student` VALUES ('2', 'stu1', '123', '123', '123', '123');

-- ----------------------------
-- Table structure for exam_paper
-- ----------------------------
DROP TABLE IF EXISTS `exam_paper`;
CREATE TABLE `exam_paper` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `name` varchar(255) DEFAULT NULL COMMENT '试卷名',
                              `levelid` bigint(2) DEFAULT NULL,
                              `status` int(5) DEFAULT NULL COMMENT '试卷状态 0为无效  1为有效',
                              `startTime` datetime DEFAULT NULL COMMENT '开始时间',
                              `endTime` datetime DEFAULT NULL COMMENT '结束时间',
                              `createTime` datetime DEFAULT NULL,
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10026 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of exam_paper
-- ----------------------------
INSERT INTO `exam_paper` VALUES ('10014', 'Java测试试卷一', null, '0', '2020-05-30 13:10:00', '2020-05-30 13:30:00', '2020-05-30 15:45:51');
INSERT INTO `exam_paper` VALUES ('10015', 'Java基础测试卷一', '2', '0', '2020-06-02 09:00:00', '2020-07-09 10:00:00', '2020-06-02 15:20:40');
INSERT INTO `exam_paper` VALUES ('10016', 'H5前端测试试卷', null, '0', '2020-10-28 22:49:28', '2020-10-28 23:50:28', '2020-08-28 22:40:28');
INSERT INTO `exam_paper` VALUES ('10019', '测试试卷', '1', '0', '2020-08-29 13:00:00', '2021-12-12 21:00:00', '2020-08-29 19:49:38');
INSERT INTO `exam_paper` VALUES ('10022', 'java测试2', '3', '0', '2021-09-01 00:00:00', '2021-12-17 00:00:00', '2021-09-13 14:38:24');
INSERT INTO `exam_paper` VALUES ('10023', 'ceshi1', '1', '0', '2021-09-30 00:00:00', '2021-10-02 00:00:00', '2021-09-30 10:45:17');
INSERT INTO `exam_paper` VALUES ('10024', '测试1', '2', '0', '2021-10-01 00:00:00', '2021-10-05 00:00:00', '2021-10-04 17:20:48');
INSERT INTO `exam_paper` VALUES ('10025', '高数期末卷一', '1', '0', '2021-10-27 16:00:00', '2021-10-27 18:00:00', '2021-10-27 11:05:48');

-- ----------------------------
-- Table structure for exam_paper_question
-- ----------------------------
DROP TABLE IF EXISTS `exam_paper_question`;
CREATE TABLE `exam_paper_question` (
                                       `id` bigint(11) NOT NULL AUTO_INCREMENT,
                                       `paperId` bigint(11) DEFAULT NULL,
                                       `questionId` bigint(11) DEFAULT NULL,
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=392 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of exam_paper_question
-- ----------------------------
INSERT INTO `exam_paper_question` VALUES ('267', '10014', '167');
INSERT INTO `exam_paper_question` VALUES ('268', '10014', '166');
INSERT INTO `exam_paper_question` VALUES ('269', '10014', '165');
INSERT INTO `exam_paper_question` VALUES ('270', '10014', '164');
INSERT INTO `exam_paper_question` VALUES ('271', '10014', '162');
INSERT INTO `exam_paper_question` VALUES ('280', '10015', '171');
INSERT INTO `exam_paper_question` VALUES ('281', '10015', '170');
INSERT INTO `exam_paper_question` VALUES ('282', '10015', '169');
INSERT INTO `exam_paper_question` VALUES ('283', '10015', '168');
INSERT INTO `exam_paper_question` VALUES ('284', '10015', '167');
INSERT INTO `exam_paper_question` VALUES ('285', '10015', '166');
INSERT INTO `exam_paper_question` VALUES ('286', '10015', '165');
INSERT INTO `exam_paper_question` VALUES ('287', '10015', '164');
INSERT INTO `exam_paper_question` VALUES ('351', '10016', '167');
INSERT INTO `exam_paper_question` VALUES ('352', '10016', '159');
INSERT INTO `exam_paper_question` VALUES ('353', '10016', '165');
INSERT INTO `exam_paper_question` VALUES ('354', '10016', '161');
INSERT INTO `exam_paper_question` VALUES ('355', '10016', '170');
INSERT INTO `exam_paper_question` VALUES ('356', '10019', '167');
INSERT INTO `exam_paper_question` VALUES ('357', '10019', '169');
INSERT INTO `exam_paper_question` VALUES ('358', '10019', '165');
INSERT INTO `exam_paper_question` VALUES ('359', '10019', '160');
INSERT INTO `exam_paper_question` VALUES ('360', '10019', '170');
INSERT INTO `exam_paper_question` VALUES ('369', '10022', '179');
INSERT INTO `exam_paper_question` VALUES ('370', '10022', '178');
INSERT INTO `exam_paper_question` VALUES ('371', '10023', '169');
INSERT INTO `exam_paper_question` VALUES ('372', '10023', '167');
INSERT INTO `exam_paper_question` VALUES ('373', '10023', '165');
INSERT INTO `exam_paper_question` VALUES ('374', '10023', '171');
INSERT INTO `exam_paper_question` VALUES ('375', '10023', '161');
INSERT INTO `exam_paper_question` VALUES ('376', '10023', '166');
INSERT INTO `exam_paper_question` VALUES ('389', '10024', '179');
INSERT INTO `exam_paper_question` VALUES ('390', '10024', '178');
INSERT INTO `exam_paper_question` VALUES ('391', '10024', '163');

-- ----------------------------
-- Table structure for exam_questionbank
-- ----------------------------
DROP TABLE IF EXISTS `exam_questionbank`;
CREATE TABLE `exam_questionbank` (
                                     `id` bigint(11) NOT NULL AUTO_INCREMENT,
                                     `questionTitle` text COMMENT '题目',
                                     `questionAnswer` varchar(255) DEFAULT NULL COMMENT '正确答案',
                                     `q_typeid` int(5) DEFAULT NULL COMMENT '题目类型（判断 0或者选择 1）',
                                     `status` int(5) DEFAULT NULL COMMENT '状态 0 删除 1有效',
                                     `createTime` datetime DEFAULT NULL,
                                     `grade` int(10) DEFAULT NULL,
                                     `creatorId` bigint(20) DEFAULT NULL,
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of exam_questionbank
-- ----------------------------
INSERT INTO `exam_questionbank` VALUES ('160', '测试判断题1', '1', '3', '0', '2020-05-30 16:24:46', '3', '1');
INSERT INTO `exam_questionbank` VALUES ('161', '测试判断题2', '0', '3', '0', '2020-05-30 16:24:49', '3', '1');
INSERT INTO `exam_questionbank` VALUES ('162', '测试判断题3', '1', '3', '0', '2020-05-30 16:24:52', '3', '1');
INSERT INTO `exam_questionbank` VALUES ('163', '今年你多少岁?', 'A', '1', '0', '2020-05-30 20:59:50', '5', '1');
INSERT INTO `exam_questionbank` VALUES ('164', '您的女朋友是不是一个?', '1', '3', '0', '2020-05-30 21:03:08', '10', '1');
INSERT INTO `exam_questionbank` VALUES ('165', '您是不是什么时候进入公司的?', '2020年10月', '2', '0', '2020-05-30 21:04:32', '3', '1');
INSERT INTO `exam_questionbank` VALUES ('166', '请描述一下您每晚的感受?', null, '4', '0', '2020-05-30 21:05:19', '20', '1');
INSERT INTO `exam_questionbank` VALUES ('167', '您多少个女朋友?', 'D', '1', '0', '2020-05-30 21:13:29', '5', '1');
INSERT INTO `exam_questionbank` VALUES ('168', 'Java里面有几种基本类型?', 'C', '1', '0', '2020-06-02 15:17:16', '5', '18');
INSERT INTO `exam_questionbank` VALUES ('169', 'Java里面包装类型有多少个?', 'D', '1', '0', '2020-06-02 15:18:35', '5', '18');
INSERT INTO `exam_questionbank` VALUES ('170', '描述一下抽象类和接口的区别?', null, '4', '0', '2020-06-02 15:19:48', '10', '18');
INSERT INTO `exam_questionbank` VALUES ('171', 'try-catch是不是能够处理异常?', '1', '3', '0', '2020-06-02 15:22:12', '3', '18');
INSERT INTO `exam_questionbank` VALUES ('172', '今年你身体涨了多少斤？', 'B', '1', '0', '2020-08-26 10:23:16', '3', '1');
INSERT INTO `exam_questionbank` VALUES ('173', '你今年去过美国?', '0', '3', '0', '2020-08-26 10:24:33', '2', '1');
INSERT INTO `exam_questionbank` VALUES ('174', '您每天都运动些什么项目？', null, '4', '0', '2020-08-26 10:25:36', '10', '1');
INSERT INTO `exam_questionbank` VALUES ('178', 'java', 'A', '1', '0', '2021-09-13 09:39:09', '5', '1');
INSERT INTO `exam_questionbank` VALUES ('179', 'java特性', 'AB', '5', '0', '2021-09-13 10:06:48', '5', '1');

-- ----------------------------
-- Table structure for exam_questiontype
-- ----------------------------
DROP TABLE IF EXISTS `exam_questiontype`;
CREATE TABLE `exam_questiontype` (
                                     `id` bigint(2) NOT NULL AUTO_INCREMENT,
                                     `name` varchar(255) DEFAULT NULL,
                                     `desc` varchar(255) DEFAULT NULL,
                                     `typeNum` varchar(255) DEFAULT NULL,
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of exam_questiontype
-- ----------------------------
INSERT INTO `exam_questiontype` VALUES ('1', '选择题', 'xzt', 'xzt');
INSERT INTO `exam_questiontype` VALUES ('2', '填空题', 'tkt', 'tkt');
INSERT INTO `exam_questiontype` VALUES ('3', '判断题', 'pdt', 'pdt');
INSERT INTO `exam_questiontype` VALUES ('4', '简答题', 'jdt', 'jdt');
INSERT INTO `exam_questiontype` VALUES ('5', '多选题', 'xzt', 'xzt');
INSERT INTO `exam_questiontype` VALUES ('6', '测试题', 'cst', 'cst');

-- ----------------------------
-- Table structure for exam_score
-- ----------------------------
DROP TABLE IF EXISTS `exam_score`;
CREATE TABLE `exam_score` (
                              `id` bigint(2) NOT NULL AUTO_INCREMENT,
                              `stuId` bigint(2) DEFAULT NULL,
                              `paperId` bigint(2) DEFAULT NULL,
                              `totalScore` varchar(255) DEFAULT NULL,
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of exam_score
-- ----------------------------

-- ----------------------------
-- Table structure for exam_scoredetail
-- ----------------------------
DROP TABLE IF EXISTS `exam_scoredetail`;
CREATE TABLE `exam_scoredetail` (
                                    `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '分数ID',
                                    `stuId` bigint(30) DEFAULT NULL COMMENT '用户ID',
                                    `paperId` bigint(11) DEFAULT NULL,
                                    `questionId` bigint(255) DEFAULT NULL COMMENT '分数',
                                    `questionTitle` varchar(255) DEFAULT NULL,
                                    `q_typeid` bigint(2) DEFAULT NULL,
                                    `questionAnswer` varchar(2000) DEFAULT NULL,
                                    `questionScore` varchar(255) DEFAULT NULL,
                                    `correntAnswer` varchar(255) DEFAULT NULL,
                                    `correntScore` varchar(255) DEFAULT NULL,
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of exam_scoredetail
-- ----------------------------
INSERT INTO `exam_scoredetail` VALUES ('166', '1', '10019', '167', '您多少个女朋友?', '1', 'D', '5', 'D', '5');
INSERT INTO `exam_scoredetail` VALUES ('167', '1', '10019', '169', 'Java里面包装类型有多少个?', '1', '您是不是什么时候进入公司的?', '5', 'D', '0');
INSERT INTO `exam_scoredetail` VALUES ('168', '1', '10019', '165', '2', '3', '测试判断题1', '2020年10月', '你妈死了', '0');
INSERT INTO `exam_scoredetail` VALUES ('169', '1', '10019', '160', '3', '3', '描述一下抽象类和接口的区别?', '1', '0', '0');
INSERT INTO `exam_scoredetail` VALUES ('170', '1', '10022', '178', 'java', '1', 'A', '5', 'A', '5');
INSERT INTO `exam_scoredetail` VALUES ('171', '1', '10022', '179', 'java特性', '5', 'AB', '5', 'AB', '5');
INSERT INTO `exam_scoredetail` VALUES ('172', '1', '10024', '163', '今年你多少岁?', '1', 'A', '5', 'A', '5');
INSERT INTO `exam_scoredetail` VALUES ('173', '1', '10024', '178', 'java', '1', 'A', '5', 'A', '5');
INSERT INTO `exam_scoredetail` VALUES ('174', '1', '10024', '179', 'java特性', '5', 'AB', '5', 'AB', '5');
INSERT INTO `exam_scoredetail` VALUES ('175', '8', '10024', '163', '今年你多少岁?', '1', 'D', '5', 'A', '0');
INSERT INTO `exam_scoredetail` VALUES ('176', '8', '10024', '178', 'java', '1', 'D', '5', 'A', '0');
INSERT INTO `exam_scoredetail` VALUES ('177', '8', '10024', '179', 'java特性', '5', 'CD', '5', 'AB', '0');

-- ----------------------------
-- Table structure for exam_times
-- ----------------------------
DROP TABLE IF EXISTS `exam_times`;
CREATE TABLE `exam_times` (
                              `times_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
                              `testpaper_id` int(11) NOT NULL COMMENT '试卷_id',
                              `user_id` varchar(30) NOT NULL COMMENT '用户ID',
                              `data_min` double DEFAULT NULL,
                              `times_state` int(5) DEFAULT NULL COMMENT '数据状态',
                              PRIMARY KEY (`times_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of exam_times
-- ----------------------------
INSERT INTO `exam_times` VALUES ('39', '10002', '149000307', '-1', '0');
INSERT INTO `exam_times` VALUES ('40', '10001', '149000307', '-1', '0');
INSERT INTO `exam_times` VALUES ('41', '10012', '149000307', '-1', '0');
INSERT INTO `exam_times` VALUES ('42', '10012', '10000', '-1', '0');
INSERT INTO `exam_times` VALUES ('43', '10001', '10000', '-1', '0');
INSERT INTO `exam_times` VALUES ('44', '10002', '10000', '-1', '0');
INSERT INTO `exam_times` VALUES ('45', '10003', '10000', '-1', '0');
INSERT INTO `exam_times` VALUES ('46', '10011', '149000307', '-1', '0');
INSERT INTO `exam_times` VALUES ('47', '10008', '149000307', '-1', '0');
INSERT INTO `exam_times` VALUES ('48', '10013', '10000', '-1', '0');

-- ----------------------------
-- Table structure for exam_xzt_options
-- ----------------------------
DROP TABLE IF EXISTS `exam_xzt_options`;
CREATE TABLE `exam_xzt_options` (
                                    `id` bigint(2) NOT NULL AUTO_INCREMENT,
                                    `optionA` varchar(255) DEFAULT NULL,
                                    `optionB` varchar(255) DEFAULT NULL,
                                    `optionC` varchar(255) DEFAULT NULL,
                                    `optionD` varchar(255) DEFAULT NULL,
                                    `questionId` bigint(2) DEFAULT NULL,
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of exam_xzt_options
-- ----------------------------
INSERT INTO `exam_xzt_options` VALUES ('1', '18-28', '28-38', '38-48', '48岁以上', '163');
INSERT INTO `exam_xzt_options` VALUES ('2', '1个', '2个', '3个', '4个', '167');
INSERT INTO `exam_xzt_options` VALUES ('3', '6个', '4个', '8个', '10个', '168');
INSERT INTO `exam_xzt_options` VALUES ('4', '2个', '4个', '6个', '8个', '169');
INSERT INTO `exam_xzt_options` VALUES ('6', '5斤', '5-10斤', '10-15斤', '15-20斤', '172');
INSERT INTO `exam_xzt_options` VALUES ('7', '1', '2', '3', '4', '178');
INSERT INTO `exam_xzt_options` VALUES ('8', '1', '2', '3', '4', '179');

-- ----------------------------
-- Table structure for liston_chapter
-- ----------------------------
DROP TABLE IF EXISTS `liston_chapter`;
CREATE TABLE `liston_chapter` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `chapterName` varchar(255) DEFAULT NULL,
                                  `chapterSubjectId` int(11) DEFAULT NULL,
                                  `csName` varchar(100) DEFAULT NULL,
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of liston_chapter
-- ----------------------------
INSERT INTO `liston_chapter` VALUES ('1', '函数', '1', '高数一');
INSERT INTO `liston_chapter` VALUES ('2', '函数2', '1', '高数一');
INSERT INTO `liston_chapter` VALUES ('3', '函数3', '1', '高数一');
INSERT INTO `liston_chapter` VALUES ('4', '曲面积分', '2', '高数二');
INSERT INTO `liston_chapter` VALUES ('5', '曲面积分2', '2', '高数二');
INSERT INTO `liston_chapter` VALUES ('6', '曲面积分3', '2', '高数二');
INSERT INTO `liston_chapter` VALUES ('7', '数据类型', '8', 'java');
INSERT INTO `liston_chapter` VALUES ('8', '函数', '8', 'java');
INSERT INTO `liston_chapter` VALUES ('10', '功能插件', '8', 'java');
INSERT INTO `liston_chapter` VALUES ('11', '功能插件2', '8', 'java');
INSERT INTO `liston_chapter` VALUES ('13', '机器码', '10', 'C++');
INSERT INTO `liston_chapter` VALUES ('14', '输入输出', '10', 'C++');
INSERT INTO `liston_chapter` VALUES ('15', '算法', '11', '数据结构');
INSERT INTO `liston_chapter` VALUES ('16', '复杂度', '11', '数据结构');
INSERT INTO `liston_chapter` VALUES ('17', '直接选择排序', '11', '数据结构');
INSERT INTO `liston_chapter` VALUES ('18', '冒泡排序', '11', '数据结构');
INSERT INTO `liston_chapter` VALUES ('19', '归并排序', '11', '数据结构');
INSERT INTO `liston_chapter` VALUES ('20', '函数', '10', 'C++');
INSERT INTO `liston_chapter` VALUES ('21', '参数', '10', 'C++');
INSERT INTO `liston_chapter` VALUES ('23', '面向对象', '10', 'C++');
INSERT INTO `liston_chapter` VALUES ('24', '文章格式', '3', '英语一');
INSERT INTO `liston_chapter` VALUES ('25', '英语标准', '3', '英语一');
INSERT INTO `liston_chapter` VALUES ('26', '科技英语', '3', '英语一');
INSERT INTO `liston_chapter` VALUES ('27', '结构化查询', '12', '数据库');
INSERT INTO `liston_chapter` VALUES ('28', '查询', '12', '数据库');
INSERT INTO `liston_chapter` VALUES ('31', '内置函数', '12', '数据库');
INSERT INTO `liston_chapter` VALUES ('32', '存储过程', '12', '数据库');

-- ----------------------------
-- Table structure for liston_question
-- ----------------------------
DROP TABLE IF EXISTS `liston_question`;
CREATE TABLE `liston_question` (
                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                   `questionType` int(255) DEFAULT NULL,
                                   `questionChapter` int(255) DEFAULT NULL,
                                   `questionDes` varchar(2000) DEFAULT NULL,
                                   `questionOption` varchar(2000) DEFAULT NULL,
                                   `questionAns` varchar(2000) DEFAULT NULL,
                                   `questionScore` double DEFAULT NULL,
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of liston_question
-- ----------------------------
INSERT INTO `liston_question` VALUES ('7', '4', '4', '这是第四个题目。用来查找ans问题', null, '这个地方填写参考答案', '5.5');
INSERT INTO `liston_question` VALUES ('8', '4', '1', '这是第无个题目。用来查找ans问题', null, '这个地方填写参考答案', '4');
INSERT INTO `liston_question` VALUES ('12', '2', '7', '这是第liu个题目。用来查找ans问题', null, '123', '3');
INSERT INTO `liston_question` VALUES ('14', '3', '5', '这是第liu个题目。用来查找ans问题', '[{\"ans\":\"Right\",\"code\":\"0xn6a849s6\"},{\"ans\":\"Wrong\",\"code\":\"26nci0dpoe\"}]', '26nci0dpoe', '4');
INSERT INTO `liston_question` VALUES ('15', '1', '1', '这是第7个题目。用来查找ans问题', '[{\"ans\":\"ans0\",\"code\":\"604ycc4p27\"},{\"ans\":\"ans1\",\"code\":\"p94z7s35f0\"},{\"ans\":\"ans2\",\"code\":\"4nmykqg6ba\"},{\"ans\":\"ans3\",\"code\":\"cs7ifemace\"}]', 'p94z7s35f0', '3');
INSERT INTO `liston_question` VALUES ('17', '5', '7', '这是第9个题目。用来查找ans问题', '[{\"ans\":\"ans0\",\"code\":\"0ay3mmb1p4\"},{\"ans\":\"ans1\",\"code\":\"tmxguc367r\"},{\"ans\":\"ans2\",\"code\":\"ojg5m3x4hr\"},{\"ans\":\"ans3\",\"code\":\"gvwsu65mfg\"},{\"ans\":\"ans4\",\"code\":\"ylm3jtvnt5\"},{\"ans\":\"ans5\",\"code\":\"thwxzjt2lc\"}]', '0ay3mmb1p4#ojg5m3x4hr#ylm3jtvnt5', '4');
INSERT INTO `liston_question` VALUES ('18', '2', '4', '这是第9个题目。用来查找ans问题', 'null', '123', '4');
INSERT INTO `liston_question` VALUES ('19', '1', '7', '这是第10个题目。用来查找ans问题', '[{\"ans\":\"ans0\",\"code\":\"651rnkpkvn\"},{\"ans\":\"ans1\",\"code\":\"68cfzmw0xh\"},{\"ans\":\"ans2\",\"code\":\"txkeku69k8\"},{\"ans\":\"ans3\",\"code\":\"22r2a5txh4\"}]', '68cfzmw0xh', '4');
INSERT INTO `liston_question` VALUES ('20', '2', '13', '这是第11个题目。用来查找ans问题', 'null', '123', '2');
INSERT INTO `liston_question` VALUES ('21', '3', '1', '收敛的数列必有界', '[{\"ans\":\"Right\",\"code\":\"9twqvnadwo\"},{\"ans\":\"Wrong\",\"code\":\"i2w29ye5fy\"}]', '9twqvnadwo', '3');
INSERT INTO `liston_question` VALUES ('22', '3', '2', '若函数f(x)在某点可导，则|f(x)|在相同位置也可导', '[{\"ans\":\"Right\",\"code\":\"6fjd4bx739\"},{\"ans\":\"Wrong\",\"code\":\"pcszagttr5\"}]', 'pcszagttr5', '3');
INSERT INTO `liston_question` VALUES ('23', '2', '3', 'f(x-1)=x^2, f(x+1)=?', 'null', 'x^2+4x+4', '2');
INSERT INTO `liston_question` VALUES ('24', '2', '2', 'u=xy+x/y,则du=?', 'null', '(y+1/y)dx+(x-x/y^2)dy', '2');
INSERT INTO `liston_question` VALUES ('25', '3', '3', 'f(x)=|x|和f(x)=√x^2是相同的函数', '[{\"ans\":\"Right\",\"code\":\"j7uk769m7t\"},{\"ans\":\"Wrong\",\"code\":\"8kco7hbddn\"}]', 'j7uk769m7t', '2');
INSERT INTO `liston_question` VALUES ('26', '2', '3', '当x!=0时，f(x)=(e^(-2x)-1)/2，当x=0时，f(x)=a;a=?', 'null', '-2', '3');
INSERT INTO `liston_question` VALUES ('27', '1', '6', 'f(x)=1,|x|<=1;f(x)=0,|x|>1.则f(f(f(x)))=?', '[{\"ans\":\"0\",\"code\":\"ow7e63ql47\"},{\"ans\":\"1\",\"code\":\"u2y3c77z6e\"},{\"ans\":\"1,|x|<=1;0,|x|>1\",\"code\":\"0xw4b1vruj\"},{\"ans\":\"0,|x|<=1;1,|x|>1\",\"code\":\"wiqlhyml74\"}]', 'u2y3c77z6e', '3');
INSERT INTO `liston_question` VALUES ('28', '1', '6', '当x趋向于1，函数(x^2-1)/(x-1)*(1/(e^x-1))极限为', '[{\"ans\":\"等于2\",\"code\":\"gwbmshypxk\"},{\"ans\":\"等于0\",\"code\":\"uxjqw887po\"},{\"ans\":\"无穷\",\"code\":\"i0e1j2g8an\"},{\"ans\":\"不存在，但不是无穷\",\"code\":\"8hioqypr6p\"}]', '8hioqypr6p', '3');
INSERT INTO `liston_question` VALUES ('29', '1', '24', 'How _____ you?', '[{\"ans\":\"are\",\"code\":\"bfpudo6qlp\"},{\"ans\":\"is\",\"code\":\"gycnqhi35a\"},{\"ans\":\"be\",\"code\":\"vi67kyb5mp\"},{\"ans\":\"what\",\"code\":\"gqdpyj5ji2\"}]', 'bfpudo6qlp', '2');
INSERT INTO `liston_question` VALUES ('30', '2', '24', '____ are you from ?', 'null', 'Where', '2');
INSERT INTO `liston_question` VALUES ('31', '1', '25', 'Can you give me an indication ____ price?', '[{\"ans\":\"on\",\"code\":\"5fp0sn3hho\"},{\"ans\":\"of\",\"code\":\"i1rfifp0rs\"},{\"ans\":\"in\",\"code\":\"v6bktngzih\"},{\"ans\":\"by\",\"code\":\"7e0ou1r7i1\"}]', 'i1rfifp0rs', '2');
INSERT INTO `liston_question` VALUES ('32', '2', '25', 'We ? firm for reply 11 a.m tomorrow.', 'null', 'offer', '2');
INSERT INTO `liston_question` VALUES ('33', '4', '26', '翻译：条条大路通罗马', 'null', 'All roads lead to Rome', '2');
INSERT INTO `liston_question` VALUES ('34', '1', '26', 'She devised a plan () they might escape from the tightly guarded prison', '[{\"ans\":\"where as\",\"code\":\"0om6d1mdyh\"},{\"ans\":\"where in \",\"code\":\"hfd0g8fn71\"},{\"ans\":\"where by \",\"code\":\"a15cm4wu0e\"},{\"ans\":\"where about\",\"code\":\"adoppiwfgv\"}]', 'a15cm4wu0e', '2');
INSERT INTO `liston_question` VALUES ('35', '1', '26', 'Our counteroffer is', '[{\"ans\":\"on test \",\"code\":\"wvuq5izywh\"},{\"ans\":\"well founded\",\"code\":\"2ef3h4pmvk\"},{\"ans\":\"well know \",\"code\":\"1o8awgqyzu\"},{\"ans\":\"well seen\",\"code\":\"3s6izx577b\"}]', '2ef3h4pmvk', '2');

-- ----------------------------
-- Table structure for liston_subject
-- ----------------------------
DROP TABLE IF EXISTS `liston_subject`;
CREATE TABLE `liston_subject` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `subjectName` varchar(255) DEFAULT NULL,
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of liston_subject
-- ----------------------------
INSERT INTO `liston_subject` VALUES ('1', '高数一');
INSERT INTO `liston_subject` VALUES ('2', '高数二');
INSERT INTO `liston_subject` VALUES ('3', '英语一');
INSERT INTO `liston_subject` VALUES ('8', 'java');
INSERT INTO `liston_subject` VALUES ('10', 'C++');
INSERT INTO `liston_subject` VALUES ('11', '数据结构');
INSERT INTO `liston_subject` VALUES ('12', '数据库');

-- ----------------------------
-- Table structure for t_dictype
-- ----------------------------
DROP TABLE IF EXISTS `t_dictype`;
CREATE TABLE `t_dictype` (
                             `id` bigint(2) NOT NULL AUTO_INCREMENT,
                             `sn` varchar(255) DEFAULT NULL,
                             `info` varchar(255) DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_dictype
-- ----------------------------
INSERT INTO `t_dictype` VALUES ('1', 'level', '试卷等级');
INSERT INTO `t_dictype` VALUES ('2', 'category', '试卷科目分类');
INSERT INTO `t_dictype` VALUES ('4', 'test', '测试111');

-- ----------------------------
-- Table structure for t_dictype_data
-- ----------------------------
DROP TABLE IF EXISTS `t_dictype_data`;
CREATE TABLE `t_dictype_data` (
                                  `id` bigint(2) NOT NULL AUTO_INCREMENT,
                                  `name` varchar(255) DEFAULT NULL,
                                  `typeid` bigint(2) DEFAULT NULL,
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_dictype_data
-- ----------------------------
INSERT INTO `t_dictype_data` VALUES ('1', '困难', '1');
INSERT INTO `t_dictype_data` VALUES ('2', '容易', '1');
INSERT INTO `t_dictype_data` VALUES ('3', '简单', '1');
INSERT INTO `t_dictype_data` VALUES ('4', '文科', '2');
INSERT INTO `t_dictype_data` VALUES ('5', '理科', '2');

-- ----------------------------
-- Table structure for t_logs
-- ----------------------------
DROP TABLE IF EXISTS `t_logs`;
CREATE TABLE `t_logs` (
                          `id` bigint(2) NOT NULL AUTO_INCREMENT,
                          `url` varchar(255) DEFAULT NULL,
                          `http_method` varchar(255) DEFAULT NULL,
                          `ip` varchar(255) DEFAULT NULL,
                          `class_method` varchar(255) DEFAULT NULL,
                          `userid` bigint(2) DEFAULT NULL,
                          `createTime` datetime DEFAULT NULL,
                          PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3203 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_logs
-- ----------------------------
INSERT INTO `t_logs` VALUES ('3000', 'http://127.0.0.1/chapter/index', 'GET', '127.0.0.1', 'com.mz.auth.controller.ChapterController.index', '1', '2021-10-13 21:13:30');
INSERT INTO `t_logs` VALUES ('3001', 'http://127.0.0.1/showimage/229caa82-17c6-4515-9bd6-edbf10212099.png', 'GET', '127.0.0.1', 'com.mz.auth.controller.FileController.showImage', '1', '2021-10-13 21:13:30');
INSERT INTO `t_logs` VALUES ('3002', 'http://127.0.0.1/paper/listpage', 'GET', '127.0.0.1', 'com.mz.auth.controller.PaperController.listPage', '1', '2021-10-13 21:13:31');
INSERT INTO `t_logs` VALUES ('3003', 'http://127.0.0.1/dic/index', 'GET', '127.0.0.1', 'com.mz.auth.controller.DicController.index', '1', '2021-10-13 21:14:10');

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
                          `id` bigint(2) NOT NULL AUTO_INCREMENT,
                          `name` varchar(255) DEFAULT NULL,
                          `url` varchar(255) DEFAULT NULL,
                          `pid` bigint(2) DEFAULT NULL,
                          `icon` varchar(255) DEFAULT NULL,
                          PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES ('1', '系统管理', null, null, 'mdi mdi-file-outline');
INSERT INTO `t_menu` VALUES ('2', '用户维护', '/user/index', '1', null);
INSERT INTO `t_menu` VALUES ('3', '角色维护', '/role/index', '1', null);
INSERT INTO `t_menu` VALUES ('4', '权限维护', '/permission/index', '1', null);
INSERT INTO `t_menu` VALUES ('5', '菜单维护', '/menu/index', '1', null);
INSERT INTO `t_menu` VALUES ('6', '数据字典', null, null, 'mdi mdi-palette');
INSERT INTO `t_menu` VALUES ('7', '信息维护', '/dic/index', '6', null);
INSERT INTO `t_menu` VALUES ('10', '学生管理', null, null, 'mdi mdi-account-multiple');
INSERT INTO `t_menu` VALUES ('11', '学生列表', '/student/index', '10', '');
INSERT INTO `t_menu` VALUES ('12', '老师管理', null, null, 'mdi mdi-account-settings-variant');
INSERT INTO `t_menu` VALUES ('13', '老师列表', '/teacher/index', '12', '');
INSERT INTO `t_menu` VALUES ('14', '题库管理', null, null, 'mdi mdi-file');
INSERT INTO `t_menu` VALUES ('15', '题库列表', '/question/index', '14', '');
INSERT INTO `t_menu` VALUES ('16', '试卷管理', null, null, 'mdi mdi-file-document');
INSERT INTO `t_menu` VALUES ('17', '试卷列表', '/paper/index', '16', '');
INSERT INTO `t_menu` VALUES ('18', '成绩管理', null, null, 'mdi mdi-file-check');
INSERT INTO `t_menu` VALUES ('19', '成绩列表', '/score/index', '18', '');
INSERT INTO `t_menu` VALUES ('20', '报表管理', null, null, 'mdi mdi-file-chart');
INSERT INTO `t_menu` VALUES ('21', '报表列表', '/report/index', '20', '');
INSERT INTO `t_menu` VALUES ('22', '日志管理', null, null, 'mdi mdi-equal-box ');
INSERT INTO `t_menu` VALUES ('23', '日志列表', '/log/index', '22', '');
INSERT INTO `t_menu` VALUES ('25', '试卷组题', '/paper/appendQuestion', '16', null);
INSERT INTO `t_menu` VALUES ('26', '老师阅卷', '/teacher/paperExamRecord', '12', null);
INSERT INTO `t_menu` VALUES ('31', '选择题上传', '/question/upload', '14', null);
INSERT INTO `t_menu` VALUES ('33', '添加学生', '/student/add', '10', '');
INSERT INTO `t_menu` VALUES ('34', '科目管理', null, null, 'mdi mdi-bank');
INSERT INTO `t_menu` VALUES ('35', '科目列表', '/ls/index', '34', 'mdi mdi-bowling');
INSERT INTO `t_menu` VALUES ('36', '章节列表', '/lc/index', '34', 'mdi mdi-bone');
INSERT INTO `t_menu` VALUES ('37', '新题库', '/lq/index', '14', 'mdi mdi-battery-charging-80');
INSERT INTO `t_menu` VALUES ('38', '新组卷', '/lp/maker', '16', 'mdi mdi-android-head');

-- ----------------------------
-- Table structure for t_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_permission`;
CREATE TABLE `t_permission` (
                                `id` bigint(2) NOT NULL AUTO_INCREMENT,
                                `name` varchar(255) DEFAULT NULL,
                                `title` varchar(255) DEFAULT NULL,
                                `pid` bigint(2) DEFAULT NULL,
                                `menuid` varchar(255) DEFAULT NULL,
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_permission
-- ----------------------------
INSERT INTO `t_permission` VALUES ('1', 'user', '用户模块', '0', '2');
INSERT INTO `t_permission` VALUES ('7', 'role', '角色模块', '0', '3');
INSERT INTO `t_permission` VALUES ('8', 'role:add', '新增角色', '7', null);
INSERT INTO `t_permission` VALUES ('9', 'role:delete', '删除角色', '7', null);
INSERT INTO `t_permission` VALUES ('10', 'role:get', '查询角色', '7', null);
INSERT INTO `t_permission` VALUES ('11', 'role:update', '修改角色', '7', null);
INSERT INTO `t_permission` VALUES ('13', 'menu', '菜单管理', '0', '5');
INSERT INTO `t_permission` VALUES ('14', 'menu:add', '新增菜单', '13', null);
INSERT INTO `t_permission` VALUES ('15', 'user:add', '用户新增', '1', null);
INSERT INTO `t_permission` VALUES ('16', 'user:delete', '用户删除', '1', null);
INSERT INTO `t_permission` VALUES ('17', 'user:get', '用户查询', '1', null);
INSERT INTO `t_permission` VALUES ('18', 'user:update', '用户更新', '1', null);
INSERT INTO `t_permission` VALUES ('19', 'menu:delete', '菜单删除', '13', null);
INSERT INTO `t_permission` VALUES ('20', 'menu:query', '菜单查询', '13', null);
INSERT INTO `t_permission` VALUES ('21', 'menu:get', '菜单获取', '13', null);
INSERT INTO `t_permission` VALUES ('22', 'permission', '权限列表', '0', '4');
INSERT INTO `t_permission` VALUES ('23', 'dic', '数据字典信息', '0', '7');
INSERT INTO `t_permission` VALUES ('24', 'student', '学生列表', '0', '11');
INSERT INTO `t_permission` VALUES ('25', 'teacher', '老师列表', '0', '13');
INSERT INTO `t_permission` VALUES ('26', 'tk', '题库列表', '0', '15');
INSERT INTO `t_permission` VALUES ('27', 'paper', '试卷列表', '0', '17');
INSERT INTO `t_permission` VALUES ('28', 'score', '成绩列表', '0', '19');
INSERT INTO `t_permission` VALUES ('29', 'report', '报表列表', '0', '21');
INSERT INTO `t_permission` VALUES ('30', 'log', '日志列表', '0', '23');
INSERT INTO `t_permission` VALUES ('31', 'zt', '试卷组题', '0', '25');
INSERT INTO `t_permission` VALUES ('32', 'checked', '老师阅卷', '0', '26');
INSERT INTO `t_permission` VALUES ('37', 'checked:add', '新增阅卷', '32', null);
INSERT INTO `t_permission` VALUES ('38', 'tkupload', '选择题上传', '0', '31');
INSERT INTO `t_permission` VALUES ('40', 'student', '添加学生', '0', '33');
INSERT INTO `t_permission` VALUES ('41', '1', '科目列表', '0', '35');
INSERT INTO `t_permission` VALUES ('42', '2', '章节列表', '0', '36');
INSERT INTO `t_permission` VALUES ('43', '2', '新题库', '0', '37');
INSERT INTO `t_permission` VALUES ('44', '3', '新组卷', '0', '38');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
                          `id` bigint(2) NOT NULL,
                          `name` varchar(255) DEFAULT NULL,
                          `sn` varchar(255) DEFAULT NULL,
                          PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '管理员', null);
INSERT INTO `t_role` VALUES ('2', '老师权限', null);

-- ----------------------------
-- Table structure for t_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_role_permission`;
CREATE TABLE `t_role_permission` (
                                     `id` bigint(2) NOT NULL AUTO_INCREMENT,
                                     `roleid` bigint(2) DEFAULT NULL,
                                     `permissionid` bigint(2) DEFAULT NULL,
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=320 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_role_permission
-- ----------------------------
INSERT INTO `t_role_permission` VALUES ('160', '2', '23');
INSERT INTO `t_role_permission` VALUES ('161', '2', '24');
INSERT INTO `t_role_permission` VALUES ('162', '2', '25');
INSERT INTO `t_role_permission` VALUES ('163', '2', '26');
INSERT INTO `t_role_permission` VALUES ('164', '2', '27');
INSERT INTO `t_role_permission` VALUES ('165', '2', '28');
INSERT INTO `t_role_permission` VALUES ('166', '2', '29');
INSERT INTO `t_role_permission` VALUES ('167', '2', '30');
INSERT INTO `t_role_permission` VALUES ('168', '2', '31');
INSERT INTO `t_role_permission` VALUES ('169', '2', '32');
INSERT INTO `t_role_permission` VALUES ('170', '2', '39');
INSERT INTO `t_role_permission` VALUES ('288', '1', '1');
INSERT INTO `t_role_permission` VALUES ('289', '1', '7');
INSERT INTO `t_role_permission` VALUES ('290', '1', '8');
INSERT INTO `t_role_permission` VALUES ('291', '1', '9');
INSERT INTO `t_role_permission` VALUES ('292', '1', '10');
INSERT INTO `t_role_permission` VALUES ('293', '1', '11');
INSERT INTO `t_role_permission` VALUES ('294', '1', '13');
INSERT INTO `t_role_permission` VALUES ('295', '1', '14');
INSERT INTO `t_role_permission` VALUES ('296', '1', '15');
INSERT INTO `t_role_permission` VALUES ('297', '1', '16');
INSERT INTO `t_role_permission` VALUES ('298', '1', '17');
INSERT INTO `t_role_permission` VALUES ('299', '1', '18');
INSERT INTO `t_role_permission` VALUES ('300', '1', '19');
INSERT INTO `t_role_permission` VALUES ('301', '1', '20');
INSERT INTO `t_role_permission` VALUES ('302', '1', '21');
INSERT INTO `t_role_permission` VALUES ('303', '1', '22');
INSERT INTO `t_role_permission` VALUES ('304', '1', '23');
INSERT INTO `t_role_permission` VALUES ('305', '1', '24');
INSERT INTO `t_role_permission` VALUES ('306', '1', '25');
INSERT INTO `t_role_permission` VALUES ('307', '1', '26');
INSERT INTO `t_role_permission` VALUES ('308', '1', '27');
INSERT INTO `t_role_permission` VALUES ('309', '1', '28');
INSERT INTO `t_role_permission` VALUES ('310', '1', '29');
INSERT INTO `t_role_permission` VALUES ('311', '1', '30');
INSERT INTO `t_role_permission` VALUES ('312', '1', '31');
INSERT INTO `t_role_permission` VALUES ('313', '1', '32');
INSERT INTO `t_role_permission` VALUES ('314', '1', '37');
INSERT INTO `t_role_permission` VALUES ('315', '1', '40');
INSERT INTO `t_role_permission` VALUES ('316', '1', '41');
INSERT INTO `t_role_permission` VALUES ('317', '1', '42');
INSERT INTO `t_role_permission` VALUES ('318', '1', '43');
INSERT INTO `t_role_permission` VALUES ('319', '1', '44');

-- ----------------------------
-- Table structure for t_student
-- ----------------------------
DROP TABLE IF EXISTS `t_student`;
CREATE TABLE `t_student` (
                             `id` bigint(2) NOT NULL AUTO_INCREMENT,
                             `username` varchar(255) DEFAULT NULL,
                             `password` varchar(255) DEFAULT NULL,
                             `tel` varchar(255) DEFAULT NULL,
                             `email` varchar(255) DEFAULT NULL,
                             `stuNum` varchar(255) DEFAULT NULL,
                             `createTime` datetime DEFAULT NULL,
                             `nickName` varchar(255) DEFAULT NULL,
                             `stuMajor` varchar(255) DEFAULT NULL,
                             `stuGrade` varchar(255) DEFAULT NULL,
                             `stuId` varchar(255) DEFAULT NULL,
                             `examNumber` varchar(255) DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_student
-- ----------------------------
INSERT INTO `t_student` VALUES ('1', 'stu1', '123', '18800010002', 'stu1@qq.com', '10120130', null, '张三', '123', '123', '123', null);
INSERT INTO `t_student` VALUES ('8', 'qwe', '123', null, null, null, null, null, 'qwe', 'qwe', 'qwe', null);
INSERT INTO `t_student` VALUES ('9', 'stu33', '123', null, null, null, null, null, '123', '123', '12345', null);
INSERT INTO `t_student` VALUES ('10', '111111', '123', null, null, null, null, null, '111111', '111111', '111111', '1111111');

-- ----------------------------
-- Table structure for t_test
-- ----------------------------
DROP TABLE IF EXISTS `t_test`;
CREATE TABLE `t_test` (
                          `id` bigint(20) NOT NULL AUTO_INCREMENT,
                          `name` varchar(255) DEFAULT NULL,
                          PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_test
-- ----------------------------
INSERT INTO `t_test` VALUES ('1', '张三');
INSERT INTO `t_test` VALUES ('2', '李四');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
                          `id` bigint(2) NOT NULL AUTO_INCREMENT,
                          `username` varchar(255) DEFAULT NULL,
                          `password` varchar(255) DEFAULT NULL,
                          `email` varchar(255) DEFAULT NULL,
                          `tel` varchar(11) DEFAULT NULL,
                          `sex` bit(1) DEFAULT NULL,
                          `headImg` varchar(255) DEFAULT NULL,
                          `createTime` datetime DEFAULT NULL,
                          `type` bigint(2) DEFAULT NULL,
                          PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'admin', '$2a$10$YITYi7HjqT2gh8jEF6eyquR/Og0qmYBNT8cQLaEjjS92jcZHwsI9G', 'admin@qq.com', '18000010002', '\0', '229caa82-17c6-4515-9bd6-edbf10212099.png', '2020-05-15 13:49:07', '1');
INSERT INTO `t_user` VALUES ('17', 't1', '$2a$10$t7K//qDwIbaPGt6fiRVL.eXeFHBHtF4aON2VGwd3u.pOwvH8AzTBe', 't1@qq.com', '18800030005', '', '229caa82-17c6-4515-9bd6-edbf10212099.png', '2020-06-02 15:04:05', '2');

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
                               `id` bigint(2) NOT NULL AUTO_INCREMENT,
                               `userid` bigint(2) DEFAULT NULL,
                               `roleid` bigint(2) DEFAULT NULL,
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES ('1', '1', '1');
INSERT INTO `t_user_role` VALUES ('2', '1', '2');
INSERT INTO `t_user_role` VALUES ('3', '17', '2');
INSERT INTO `t_user_role` VALUES ('14', '18', '2');
INSERT INTO `t_user_role` VALUES ('15', '20', '2');

-- ----------------------------
-- View structure for v_score
-- ----------------------------
DROP VIEW IF EXISTS `v_score`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_score` AS select `es`.`stuId` AS `stuId`,`es`.`paperId` AS `paperId`,`s`.`nickName` AS `nickName`,`p`.`name` AS `name`,sum(`es`.`correntScore`) AS `totalScore` from ((`exam_paper` `p` join `exam_scoredetail` `es` on((`p`.`id` = `es`.`paperId`))) join `t_student` `s` on((`s`.`id` = `es`.`stuId`))) group by `s`.`id`,`es`.`paperId` ;

-- ----------------------------
-- View structure for v_stuscore
-- ----------------------------
DROP VIEW IF EXISTS `v_stuscore`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_stuscore` AS select `s`.`id` AS `stuId`,`ep`.`id` AS `paperId`,`s`.`nickName` AS `nickName`,`ep`.`name` AS `name`,sum(`sd`.`correntScore`) AS `totalScore` from ((`exam_scoredetail` `sd` join `exam_paper` `ep` on((`sd`.`paperId` = `ep`.`id`))) join `t_student` `s` on((`s`.`id` = `sd`.`stuId`))) group by `sd`.`paperId`,`sd`.`stuId` ;
