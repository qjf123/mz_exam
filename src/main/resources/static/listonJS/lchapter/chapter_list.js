
    $(document).ready(function(e){
    $('#paperTable').bootstrapTable({
        url: '/lc/ga',                      //请求后台的URL（*）
        method: 'GET',                      //请求方式（*）
        dataType: "json",
        cache: false,
        uniqueId: "id",                     //每一行的唯一标识，一般为主键列
        contentType: "application/x-www-form-urlencoded",
        responseHandler: function (res) {
            return {
                "rows": res.res
            };
        },
        columns: [
            {
                field: 'id',
                title: '章节编号'
            }, {
                field: 'chapterName',
                title: '章节名称'
            }, {
                field: 'csName',
                title: '科目名称'
            }, {
                field: 'chapterSubjectId',
                title: '科目编号'
            }, {
                field: 'doOpt',
                title: '操作',
                formatter : optFormatter
            }]
    });

    function optFormatter(value,row, index){
    var c = '<a class="btn btn-xs btn-default" href="#!"  onclick=\'edit("' + row.id + '")\' title="编辑" data-toggle="tooltip"><i class="mdi mdi-pencil"></i></a>';
    var e = '<div class="btn btn-xs btn-default"  href="#!" onclick="del(\''+row.id+'\')" title="删除"  data-toggle="tooltip"><i class="mdi mdi-window-close"></i><div/> ';
    return c + e ;
}

    //绑定事件
    $("button,a").on('click',function(){
    //获取到 a标签里面配置 data-method
    var methodName = $(this).data('method');
    if(methodName){
    doMethod[methodName]();
}
});

    var doMethod = {
    search:function(){
    var name = $("#q_name").val();
    console.log('name: ' + name)
    var queryparam = {
    silent:true,
    query:{
    subjectName:name
}
};

    $('#paperTable').bootstrapTable('refresh',queryparam);

},

    add:function(){
    //新增
    $("#paperAddModal").modal({show:true})
    showSel()
},

    save(){
    var myData = {}
    myData['chapterName'] = $("#a_name").val()
    myData['chapterSubjectId'] = $('#addid').val()
    myData['csName'] = $("#addid").find("option:selected").text()
    $.ajax({
    type:'POST',
    url:'/lc/an',
    dataType:'json',
    contentType:'application/json;charset=UTF-8',
    data:JSON.stringify(myData),
    success:function(data){
    if(data.code === 0){
    $.confirm({
    title:'温馨提示',
    content:'保存成功',
    type:'green',
    buttons:{
    ok:{
    text:'谢谢',
    btnClass:'btn-green'
}
}
})
    $("#paperAddModal").modal('hide')
    $("#paperTable").bootstrapTable('refresh')
}else{
    $.confirm({
    title:'温馨提示',
    content:'操作失败'+data.msg,
    type:'red',
    buttons:{
    ok:{
    text:'谢谢',
    btnClass:'btn-red'
}
}
})
}
}
})

},

    editSave:function(){
    var myData = {}
    myData['id'] = $("#e_id").val()
    myData['chapterName'] = $("#e_name").val()
    $.ajax({
    type:'POST',
    url:'/lc/uname',
    dataType:'json',
    contentType:'application/json;charset=UTF-8',
    data:JSON.stringify(myData),
    success:function(data){
    if(data.code === 0){
    $.confirm({
    title:'温馨提示',
    content:'保存成功',
    type:'green',
    buttons:{
    ok:{
    text:'谢谢',
    btnClass:'btn-green'
}
}
})
    $("#paperEditModal").modal('hide')
    $("#paperTable").bootstrapTable('refresh')
}else{
    $.confirm({
    title:'温馨提示',
    content:'操作失败'+data.message,
    type:'red',
    buttons:{
    ok:{
    text:'谢谢',
    btnClass:'btn-red'
}
}
})
}
}
})

}

}
});

    //////////////////////////////////////////////////////修改操作////////////////////////////////////
    function edit(id){
    //显示修改的对话框
    $("#paperEditModal").modal({show:true})
    //数据的回显
    var editRow = $("#paperTable").bootstrapTable('getRowByUniqueId',id)
    $("#e_id").val(editRow.id)
    $("#e_name").val(editRow.name)

}

    //////////////////////////////////////////////////////删除操作////////////////////////////////////
    function del(id){
    $.confirm({
        title:'温馨提示',
        content:'确认删除?',
        type:'green',
        buttons:{
            ok:{
                text:'确定',
                btnClass:'btn-green',
                action:function(){
                    //根据id 删除数据
                    $.post("/lc/dyid",{"id":id},function(data){
                        if(data.code===0){
                            $.confirm({
                                title:'温馨提示',
                                content:'删除成功',
                                type:'green',
                                buttons:{
                                    omg:{
                                        text:'谢谢',
                                        btnClass:'btn-green'
                                    }
                                }
                            })
                            //刷新数据
                            $("#paperTable").bootstrapTable('refresh');
                        }else{
                            $.confirm({
                                title:'温馨提示',
                                content:'删除失败. ' + data.msg,
                                type:'red',
                                buttons:{
                                    omg:{
                                        text:'关闭',
                                        btnClass:'btn-red'
                                    }
                                }
                            })
                        }
                    })

                }
            },
            cancel:{
                text:'取消',
                btnClass:'btn-red'
            }
        }
    })
}

    function showSel(){
    $("#addid").empty()
    $.ajax({
    type:'POST',
    url:'/ls/ga',
    dataType:'json',
    contentType:'application/json;charset=UTF-8',
    success : function(data) {
    var subject_list = data.res;
    var opts = "";
    // $("#addid").append("<option value='ope'>请选择</option>")
    for( var subject_index = 0 ; subject_index < subject_list.length; subject_index++ ){
    var subject = subject_list[subject_index];
    opts += "<option value='"+subject.id+"'>"+subject.subjectName+"</option>";
}
    $("#addid").append(opts);
    $("#addid").selectpicker("refresh");
}
});
}
