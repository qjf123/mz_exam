// 查询所有的paper
function showAllPaper(){
    $("#selectPaper").empty()
    $.ajax({
        type: 'POST',
        url: '/lp/ga',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        success: function (data) {
            var paper_list = data.res;
            var opts = "<option value=\"\" style=\"display: none;\" disabled selected>选择试卷</option>";
            // $("#addid").append("<option value='ope'>请选择</option>")
            for (var index = 0; index < paper_list.length; index++) {
                var paper = paper_list[index];
                opts += "<option value='" + paper.id + "'>" + paper.name + "</option>";
            }
            $("#selectPaper").append(opts);
            $("#selectPaper").selectpicker("refresh");
        }
    });
}

// 查询所有的科目
function showSelAllSubject() {
    $("#selectSubject").empty()
    $.ajax({
        type: 'POST',
        url: '/ls/ga',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        success: function (data) {
            var subject_list = data.res;
            var opts = "<option value=\"\" style=\"display: none;\" disabled selected>选择科目</option>";
            // $("#addid").append("<option value='ope'>请选择</option>")
            for (var subject_index = 0; subject_index < subject_list.length; subject_index++) {
                var subject = subject_list[subject_index];
                opts += "<option value='" + subject.id + "'>" + subject.subjectName + "</option>";
            }
            $("#selectSubject").append(opts);
            $("#selectSubject").selectpicker("refresh");
        }
    });
}

// 完成选择科目
function changeSubject() {
    $("#selectChapter").empty()
    var subjectId = $("#selectSubject").val()
    $.ajax({
        type: 'POST',
        url: '/lc/gysid',
        // dataType:'json',
        // contentType:'application/json;charset=UTF-8',
        data: {"subjectId": subjectId},
        // data: subjectId,
        success: function (data) {
            var chapter_list = data.res;
            var opts = "<option value=\"\" style=\"display: none;\" disabled selected>选择章节</option>";
            opts += "<option value=\"all\">所有章节</option>";
            // $("#addid").append("<option value='ope'>请选择</option>")
            for (var index = 0; index < chapter_list.length; index++) {
                var chapter = chapter_list[index];
                opts += "<option value='" + chapter.id + "'>" + chapter.chapterName + "</option>";
            }
            $("#selectChapter").append(opts);
            $("#selectChapter").selectpicker("refresh");
        }
    });
}

// 根据选择框内的数据，查询试题
function showQuestionByChapter() {
    var subjectId = $("#selectChapter").val()
    var get_url = '/lq/gci?questionChapter=' + subjectId
    var get_para = {'questionChapter': subjectId}
    if ( subjectId == 'all' ) {
        get_url = '/lq/gsi?questionSubject=' + $("#selectSubject").val()
        get_para = {'questionSubject': $("#selectSubject").val()}
    }
    // $('#questionTable').html("")
    $.ajax({
        url: get_url,
        type: "POST",
        success: function (data) {
            $('#questionTable').bootstrapTable('removeAll');
            $('#questionTable').bootstrapTable('append', data.res);
            $('#questionTable').bootstrapTable('hideColumn', 'id');
            $('#questionTable').bootstrapTable('hideColumn', 'questionSubjectStr');
            $('#questionTable').bootstrapTable('hideColumn', 'questionChapterStr');
        }
    });
}

// 下拉框选择试卷后，显示信息
function showPaperInfo(){
    $('#paperInfoTable').bootstrapTable({
        url: '/lq/ga',                      //请求后台的URL（*）
        method: 'GET',                      //请求方式（*）
        dataType: "json",
        cache: false,
        uniqueId: "id",                     //每一行的唯一标识，一般为主键列
        contentType: "application/x-www-form-urlencoded",
        responseHandler: function (res) {
            return {
                "rows": res.res
            };
        },
        columns: [
            {
                field: 'questionTypeStr',
                title: '类型'
            },{
                field: 'questionScore',
                title: '分值'
            }, {
                field: 'questionDes',
                title: '描述'
            }, {
                field: 'doOpt',
                title: '操作',
                formatter: removeFromPaperOpt
            }]
    });
    function removeFromPaperOpt(value, row, index) {
        var e = '<div class="btn btn-xs btn-default"  href="#!" onclick="removeFromPaper(\'' + row.id + '\')" ' +
            'title="删除"  data-toggle="tooltip"><i class="mdi mdi-window-close"></i><div/> ';
        return e;
    }
}

//#########################初始化问题列表表格######################
$(document).ready(function(e){
    showAllPaper()
    showSelAllSubject()
    $('#questionTable').bootstrapTable({
        url: '/lq/ga',                      //请求后台的URL（*）
        method: 'GET',                      //请求方式（*）
        dataType: "json",
        cache: false,
        uniqueId: "id",                     //每一行的唯一标识，一般为主键列
        contentType: "application/x-www-form-urlencoded",
        responseHandler: function (res) {
            return {
                "rows": res.res
            };
        },
        columns: [
            {
                field: 'questionTypeStr',
                title: '类型'
            }, {
                field: 'questionSubjectStr',
                title: '科目'
            },  {
                field: 'questionScore',
                title: '分值'
            }, {
                field: 'questionDes',
                title: '描述'
            }, {
                field: 'questionAnsStr',
                title: '答案'
            },  {
                field: 'doOpt',
                title: '操作',
                formatter: addToPaperOpt
            }]
    });
    function addToPaperOpt(value, row, index) {
        var e = '<div class="btn btn-xs btn-default"  href="#!" onclick="addToPaper(\'' + row.id + '\')" ' +
            'title="增加题目"  data-toggle="tooltip"><i class="glyphicon glyphicon-circle-arrow-right"></i><div/> ';
        return e;
    }
    //绑定事件
    $("button,a").on('click',function(){
        //获取到 a标签里面配置 data-method
        var methodName = $(this).data('method');
        if(methodName){
            doMethod[methodName]();
        }
    });
    var doMethod = {
        search:function(){
            var name = $("#q_name").val();
            var queryparam = {
                silent:true,
                query:{
                    name:name
                }
            };
            $('#questionTable').bootstrapTable('refresh',queryparam);
        },
        randomPaperQuestion(){
            //随机组卷 判断一下用户是否已经选择的试卷 如果选择，提示用户选择左侧试卷
            if(!nodeId){
                //表示没有选中左侧试卷树
                $.confirm({
                    title:'温馨提示',
                    content:'亲,请选中左侧试卷树进行组卷操作',
                    type:'green',
                    buttons:{
                        ok:{
                            text:"谢谢",
                            btnClass:'btn-green'
                        }
                    }
                })

                return
            }

            //到后台查询对应类型的题型 有多少 作为一个参数填写
            $.ajax({
                url:'/paper/queryTypeTotal',
                type:'POST',
                contentType:'application/json;charset=utf-8',
                success:function(data){
                    // [{q_typeid:1,totalNum:10,q_typeid:2,totalNum:20}]
                    $("#sj_paperid").val(nodeId)
                    for(var i=0;i<data.length;i++){
                        let typeTotalVo = data[i]
                        switch(typeTotalVo.q_typeid){
                            case 1:$("#xztTotalId").html(typeTotalVo.totalNum); break;
                            case 2:$("#tktTotalId").html(typeTotalVo.totalNum); break;
                            case 3:$("#pdtTotalId").html(typeTotalVo.totalNum); break;
                            case 4:$("#jdtTotalId").html(typeTotalVo.totalNum); break;
                            case 5:$("#dxtTotalId").html(typeTotalVo.totalNum); break;
                        }
                    }
                    //弹出对话框
                    $("#randomZJModal").modal({show:true})
                }
            })

        },
        sjZJSave(){
            //获取表单里面的参数
            var sendObj={
                "paperId":$("#sj_paperid").val(),
                "xztNum":$("#sj_xzt").val(),
                "tktNum":$("#sj_tkt").val(),
                "pdtNum":$("#sj_pdt").val(),
                "jdtNum":$("#sj_jdt").val(),
                "dxtNum":$("#sj_dxt").val()
            }
            $.ajax({
                url:"/paper/randomPaperQuestion",
                type:"POST",
                dataType:"json",
                data:JSON.stringify(sendObj),
                contentType:"application/json;charset=utf-8",
                success:function(data){
                    if(data.isSuccess){
                        $.confirm({
                            title:'温馨提示',
                            content:'组卷成功',
                            type:'green',
                            buttons:{
                                ok:{
                                    text:"谢谢",
                                    btnClass:'btn-green',
                                    action:function(){
                                        $("#randomZJModal").modal('hide')
                                        location.href='/paper/appendQuestion'
                                    }
                                }
                            }
                        })
                    }else{
                        $.confirm({
                            title:'温馨提示',
                            content:'组卷失败'+data.message,
                            type:'red',
                            buttons:{
                                ok:{
                                    text:"谢谢",
                                    btnClass:'btn-red'
                                }
                            }
                        })
                    }
                }
            })

        },
        diyPaperQuestion(){
            //手动组卷
            if(!nodeId){
                //表示没有选中左侧试卷树
                $.confirm({
                    title:'温馨提示',
                    content:'亲,请选中左侧试卷树名称',
                    type:'green',
                    buttons:{
                        ok:{
                            text:"谢谢",
                            btnClass:'btn-green'
                        }
                    }
                })

                return
            }

            //获取表格里面选中数据
            var rows =  $("#questionTable").bootstrapTable('getAllSelections')
            if(rows.length == 0){
                $.confirm({
                    title:'温馨提示',
                    content:'亲,请选中右侧的试题在进行组卷操作',
                    type:'green',
                    buttons:{
                        ok:{
                            text:"谢谢",
                            btnClass:'btn-green'
                        }
                    }
                })
                return
            }

            //把参数传递到后台 完成组卷
            var questionIdsList = rows.map(item=>{
                return {id:item.id}
            })
            //[{paperId:10015,questionIdsList:[{id:111},{id:112}]}]
            var sendObj = {
                paperId:nodeId,
                questionIdsList:questionIdsList
            }
            $.ajax({
                url:"/paper/diyPaperQuestion",
                type:"POST",
                dataType:"json",
                data:JSON.stringify(sendObj),
                contentType:"application/json;charset=utf-8",
                success:function(data){
                    if(data.isSuccess){
                        $.confirm({
                            title:'温馨提示',
                            content:'保存成功',
                            type:'green',
                            buttons:{
                                ok:{
                                    text:"谢谢",
                                    btnClass:'btn-green',
                                    action:function(){
                                        location.href="/paper/appendQuestion";
                                    }
                                }
                            }
                        })
                    }else{
                        $.confirm({
                            title:'温馨提示',
                            content:'保存失败'+data.message,
                            type:'red',
                            buttons:{
                                ok:{
                                    text:"关闭",
                                    btnClass:'btn-red'
                                }
                            }
                        })
                    }
                }
            })



        },
        previewPaper(){
            //预览试卷方法
            if(!nodeId){
                //表示没有选中左侧试卷树
                $.confirm({
                    title:'温馨提示',
                    content:'亲,请选中左侧试卷树进行预览操作',
                    type:'green',
                    buttons:{
                        ok:{
                            text:"谢谢",
                            btnClass:'btn-green'
                        }
                    }
                })
                return
            }
            //根据试卷id 查询预览的试卷信息 跳转预览页面 完成预览
            location.href='/paper/previewPaper/'+nodeId
        },
        savePaper(){
            console.log("save paper!")
        }
    }
});

function removeFromPaper(id) {
    console.log("remove question from paper!")
}

function addToPaper(id){
    console.log("add question to paper!")
}
