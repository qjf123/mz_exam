let danXuanIndex = 0
let duoXuanIndex = 0

function showSel() {
    $("#selectSubject").empty()
    $.ajax({
        type: 'POST',
        url: '/ls/ga',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        success: function (data) {
            var subject_list = data.res;
            var opts = "";
            // $("#addid").append("<option value='ope'>请选择</option>")
            for (var subject_index = 0; subject_index < subject_list.length; subject_index++) {
                var subject = subject_list[subject_index];
                opts += "<option value='" + subject.id + "'>" + subject.subjectName + "</option>";
            }
            $("#selectSubject").append(opts);
            $("#selectSubject").selectpicker("refresh");
        }
    });
}

function showQuestionType() {
    $("#selectSubject").empty()
    $.ajax({
        type: 'POST',
        url: '/lq/gat',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        success: function (data) {
            var type_list = data.res;
            var opts = "";
            // $("#addid").append("<option value='ope'>请选择</option>")
            for (var index = 0; index < type_list.length; index++) {
                var type = type_list[index];
                opts += "<option value='" + type.id + "'>" + type.name + "</option>";
            }
            $("#queType").append(opts);
            $("#queType").selectpicker("refresh");
        }
    });
}

function changeSubject() {
    $("#selectChapter").empty()
    var subjectId = $("#selectSubject").val()
    console.log("selectSubject: " + subjectId)
    // myData['csName'] = $("#addid").find("option:selected").text()
    $.ajax({
        type: 'POST',
        url: '/lc/gysid',
        // dataType:'json',
        // contentType:'application/json;charset=UTF-8',
        data: {"subjectId": subjectId},
        // data: subjectId,
        success: function (data) {
            var chapter_list = data.res;
            var opts = "";
            // $("#addid").append("<option value='ope'>请选择</option>")
            for (var index = 0; index < chapter_list.length; index++) {
                var chapter = chapter_list[index];
                opts += "<option value='" + chapter.id + "'>" + chapter.chapterName + "</option>";
            }
            $("#selectChapter").append(opts);
            $("#selectChapter").selectpicker("refresh");
        }
    });
}

function addDanXuanRow() {
    var optId = "optionsRadios" + danXuanIndex
    var optval = "option" + danXuanIndex
    var ans = "ans" + danXuanIndex

    var addRow = "<div class=\"radio\">\n" +
        " <label>\n" +
        " <input type=\"radio\" name=\"dxtRadios\" id=\"" + optId + "\" value=\"" + optval + "\">\n" +
        " <input type=\"text\" name=\"dxtTexts\" class=\"form-control\" id=\"" + ans + "\" value=\"" + ans + "\" placeholder=\"" + ans +
        " \" aria-describedby=\"basic-addon1\">\n" +
        " </label>\n" +
        " </div>"
    danXuanIndex = danXuanIndex + 1
    $('#xztRadios').append(addRow)
}

function addDuoXuanRow() {
    var optId = "checkbox" + duoXuanIndex
    var ans = "ans" + duoXuanIndex

    var addRow = "<div class=\"checkbox\">\n" +
        " <label>\n" +
        " <input type=\"checkbox\" name=\"mcCheckboxs\" id=\"" + optId + "\" value=\"" + optId + "\">\n" +
        " <input type=\"text\" name=\"mcTexts\" class=\"form-control\" id=\"" + ans + "\" value=\"" + ans + "\" placeholder=\"" + ans +
        " \" aria-describedby=\"basic-addon1\">\n" +
        " </label>\n" +
        " </div>"
    duoXuanIndex = duoXuanIndex + 1
    $('#dxtCheckboxs').append(addRow)
}

//当选择类型下来改变的时候 ，出现对应的模板
function changeType() {
    var typeId = $("#queType").val()
    switch (typeId) {
        case '1':
            $("#xztRadios").html("")
            danXuanIndex = 0
            $("#xztArea").show();
            $("#tktArea").hide();
            $("#pdtArea").hide();
            $("#jdtArea").hide();
            $("#dxtArea").hide();
            break;
        case '2':
            $("#tktArea").show();
            $("#xztArea").hide();
            $("#pdtArea").hide();
            $("#jdtArea").hide();
            $("#dxtArea").hide();
            break;
        case '3':
            $("#pdtArea").show();
            $("#xztArea").hide();
            $("#tktArea").hide();
            $("#jdtArea").hide();
            $("#dxtArea").hide();
            break;
        case '4':
            $("#jdtArea").show();
            $("#xztArea").hide();
            $("#tktArea").hide();
            $("#pdtArea").hide();
            $("#dxtArea").hide();
            break;
        case '5':
            $("#dxtCheckboxs").html("")
            duoXuanIndex = 0
            $("#xztArea").hide();
            $("#tktArea").hide();
            $("#pdtArea").hide();
            $("#jdtArea").hide();
            $("#dxtArea").show();
            break;
    }

}

function randomString(n) {
    let str = 'abcdefghijklmnopqrstuvwxyz9876543210';
    let tmp = '',
        i = 0,
        l = str.length;
    for (i = 0; i < n; i++) {
        tmp += str.charAt(Math.floor(Math.random() * l));
    }
    return tmp;
}

$(document).ready(function (e) {
    console.log("enter ajax")
    showSel()
    showQuestionType()
    $("#xztArea").show();
    //绑定事件
    $("button,a").on('click', function () {
        //获取到 a标签里面配置 data-method
        var methodName = $(this).data('method');
        if ( methodName ) {
            doMethod[methodName]();
        }
    });
    var doMethod = {
        /**
         * 保存问题的方法
         */
        save() {
            var my_data = {}
            var questionType = $("#queType").find("option:selected").val()
            my_data['questionType'] = questionType
            my_data['questionScore'] = $("#grade").val()
            my_data['questionChapter'] = $("#selectChapter").val()
            my_data['questionDes'] = $("#questionDes").val()

            if ( questionType === '1' ) {        // 单选题
                var dxtRadios = document.getElementsByName("dxtRadios")
                var dxtTexts = document.getElementsByName("dxtTexts")
                optList = []
                for (var i = 0; i < dxtRadios.length; i++) {
                    tempData = {}
                    var ansCode = randomString(10)
                    tempData['ans'] = dxtTexts[i].value
                    tempData['code'] = ansCode
                    if ( dxtRadios[i].checked ) {
                        my_data['questionAns'] = ansCode
                    }
                    optList.push(tempData)
                }
                my_data['listonQuestionItemList'] = optList
            } else if ( questionType === '2' ) {   // 填空题
                my_data['questionAns'] = $("#tktAns").val()
            } else if ( questionType === '3' ) {   // 判断题
                var pdtRadios = document.getElementsByName("pdtAns")
                optList = []
                for (var i = 0; i < pdtRadios.length; i++) {
                    tempData = {}
                    var ansCode = randomString(10)
                    tempData['ans'] = pdtRadios[i].value
                    tempData['code'] = ansCode
                    if ( pdtRadios[i].checked ) {
                        my_data['questionAns'] = ansCode
                    }
                    optList.push(tempData)
                }
                my_data['listonQuestionItemList'] = optList
            } else if ( questionType === '4' ) {   // 简答题
                                                   // 这个地方要注意参数的类型。应该是字符串类型，而不是int
                my_data['questionAns'] = $("#jdtAns").val()
            } else if ( questionType === '5' ) {   // 多选题
                var dxtRadios = document.getElementsByName("mcCheckboxs")
                var dxtTexts = document.getElementsByName("mcTexts")
                optList = []
                ans_list = []

                for (var i = 0; i < dxtRadios.length; i++) {
                    tempData = {}
                    var ansCode = randomString(10)
                    tempData['ans'] = dxtTexts[i].value
                    tempData['code'] = ansCode
                    if ( dxtRadios[i].checked ) {
                        ans_list.push(ansCode)
                    }
                    optList.push(tempData)
                }
                my_data['questionAns'] = ans_list.join("#")
                my_data['listonQuestionItemList'] = optList
            }
            // console.log(JSON.stringify(my_data))
            //发送ajax请求到后台
            $.ajax({
                url: "/lq/an",
                type: "POST",
                dataType: "json",
                data: JSON.stringify(my_data),
                contentType: 'application/json;charset=utf-8',
                success: function (data) {
                    if ( data.code === 0 ) {
                        $.confirm({
                            title: "温馨提示:",
                            content: "保存成功",
                            type: 'green',
                            buttons: {
                                ok: {
                                    text: '谢谢',
                                    btnClass: 'btn-green',
                                    action: function () {
                                        location.href = '/lq/index'
                                    }
                                }
                            }
                        })

                    } else {
                        $.confirm({
                            title: "温馨提示:",
                            content: "保存失败" + data.msg,
                            type: 'red',
                            button: {
                                ok: {
                                    text: '关闭',
                                    btnClass: 'btn-red'
                                }
                            }
                        })
                    }
                }
            })

        }
    }
});
