// 查询所有的科目
function showSelAllSubject() {
    $("#selectSubject").empty()
    $.ajax({
        type: 'POST',
        url: '/ls/ga',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        success: function (data) {
            var subject_list = data.res;
            var opts = "<option value=\"\" style=\"display: none;\" disabled selected>选择科目</option>";
            // $("#addid").append("<option value='ope'>请选择</option>")
            for (var subject_index = 0; subject_index < subject_list.length; subject_index++) {
                var subject = subject_list[subject_index];
                opts += "<option value='" + subject.id + "'>" + subject.subjectName + "</option>";
            }
            $("#selectSubject").append(opts);
            $("#selectSubject").selectpicker("refresh");
        }
    });
}

// 完成选择科目
function changeSubject() {
    $("#selectChapter").empty()
    var subjectId = $("#selectSubject").val()
    // console.log("selectSubject: " + subjectId)
    // myData['csName'] = $("#addid").find("option:selected").text()
    $.ajax({
        type: 'POST',
        url: '/lc/gysid',
        // dataType:'json',
        // contentType:'application/json;charset=UTF-8',
        data: {"subjectId": subjectId},
        // data: subjectId,
        success: function (data) {
            var chapter_list = data.res;
            var opts = "<option value=\"\" style=\"display: none;\" disabled selected>选择章节</option>";
            opts += "<option value=\"all\">所有章节</option>";
            for (var index = 0; index < chapter_list.length; index++) {
                var chapter = chapter_list[index];
                opts += "<option value='" + chapter.id + "'>" + chapter.chapterName + "</option>";
            }
            $("#selectChapter").append(opts);
            $("#selectChapter").selectpicker("refresh");
        }
    });
}

// 根据选择框内的数据，查询试题
function showQuestionByChapter() {
    var subjectId = $("#selectChapter").val()
    var get_url = '/lq/gci?questionChapter=' + subjectId
    var get_para = {'questionChapter': subjectId}
    if ( subjectId == 'all' ) {
        get_url = '/lq/gsi?questionSubject=' + $("#selectSubject").val()
        get_para = {'questionSubject': $("#selectSubject").val()}
    }
    // $('#questionTable').html("")
    $.ajax({
        url: get_url,
        type: "POST",
        success: function (data) {
            $('#questionTable').bootstrapTable('removeAll');
            $('#questionTable').bootstrapTable('append', data.res);
            $('#questionTable').bootstrapTable('hideColumn', 'id');
            $('#questionTable').bootstrapTable('hideColumn', 'questionSubjectStr');
            $('#questionTable').bootstrapTable('hideColumn', 'questionChapterStr');
        }
    });
}

$(document).ready(function (e) {
    showSelAllSubject()
    $('#questionTable').bootstrapTable({
        url: '/lq/ga',                      //请求后台的URL（*）
        method: 'GET',                      //请求方式（*）
        dataType: "json",
        cache: false,
        uniqueId: "id",                     //每一行的唯一标识，一般为主键列
        contentType: "application/x-www-form-urlencoded",
        responseHandler: function (res) {
            return {
                "rows": res.res
            };
        },
        columns: [
            {
                field: 'id',
                title: '编号'
            }, {
                field: 'questionTypeStr',
                title: '类型'
            }, {
                field: 'questionSubjectStr',
                title: '科目'
            }, {
                field: 'questionChapterStr',
                title: '章节'
            }, {
                field: 'questionScore',
                title: '分值'
            }, {
                field: 'questionDes',
                title: '描述'
            }, {
                field: 'questionAnsStr',
                title: '答案'
            }, {
                field: 'questionOptionStr',
                title: '选项'
            }, {
                field: 'doOpt',
                title: '操作',
                formatter: optFormatter
            }]
    });

    function optFormatter(value, row, index) {
        var c = '<a class="btn btn-xs btn-default" href="#!"  onclick=\'edit("' + row.id + '")\' title="编辑" data-toggle="tooltip"><i class="mdi mdi-pencil"></i></a>';
        var e = '<div class="btn btn-xs btn-default"  href="#!" onclick="del(\'' + row.id + '\')" title="删除"  data-toggle="tooltip"><i class="mdi mdi-window-close"></i><div/> ';
        // return c + e ;
        return e;
    }

    //绑定事件
    $("button,a").on('click', function () {
        //获取到 a标签里面配置 data-method
        var methodName = $(this).data('method');
        if ( methodName ) {
            doMethod[ methodName ]();
        }
    });

    var doMethod = {
        search: function () {
            //查询方法
            var name = $("#q_name").val();
            var queryparam = {
                query: {
                    questionTitle: name
                }
            };
            $('#questionTable').bootstrapTable('refresh', queryparam);

        },
        add: function () {
            //弹出新增问题的页面
            location.href = "/lq/gotoan"
        }
    }
});

//////////////////////////////////////////////////////修改操作////////////////////////////////////
function edit(id) {
    location.href = "/question/gotoEditQuestion/" + id
}

//////////////////////////////////////////////////////删除操作////////////////////////////////////
function del(id) {
    $.confirm({
        title: '温馨提示',
        content: '删除数据吗?',
        type: 'green',
        buttons: {
            ok: {
                text: '确定',
                btnClass: 'btn-green',
                action: function () {
                    //根据id 删除数据
                    $.get("/lq/dyid", {"id": id}, function (data) {
                        if ( data.code === 0 ) {
                            $.confirm({
                                title: '温馨提示',
                                content: '删除成功',
                                type: 'green',
                                buttons: {
                                    omg: {
                                        text: '谢谢',
                                        btnClass: 'btn-green'
                                    }
                                }
                            })
                            //刷新数据
                            $("#questionTable").bootstrapTable('refresh');
                        } else {
                            $.confirm({
                                title: '温馨提示',
                                content: '删除失败. ' + data.msg,
                                type: 'red',
                                buttons: {
                                    omg: {
                                        text: '关闭',
                                        btnClass: 'btn-red'
                                    }
                                }
                            })
                        }
                    })

                }
            },
            cancel: {
                text: '取消',
                btnClass: 'btn-red'
            }
        }
    })


}