package com.liston.exam.liston.paper;

import com.liston.exam.entity.Paper;
import com.liston.exam.liston.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListonPaperService {
    @Autowired
    ListonPaperMapper listonPaperMapper;

    public List<Paper> getAllPaper() throws ListonPaperException {
        List<Paper> ret = null;
        try {
            ret = listonPaperMapper.selectAllPaper();
        }
        catch (Exception ex){
            throw new ListonPaperException(Constant.ListonPaperError.SELECT_ALL_ERROR_MSG,
                    Constant.ListonPaperError.SELECT_ALL_ERROR_CODE);
        }
        return ret;
    }
}
