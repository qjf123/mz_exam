package com.liston.exam.liston.paper;

import com.liston.exam.entity.Paper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ListonPaperMapper {
    @Select("select * from exam_paper")
    List<Paper> selectAllPaper();
}
