package com.liston.exam.liston.paper;

import com.liston.exam.entity.Paper;
import com.liston.exam.liston.Constant;
import com.liston.exam.liston.ListonResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ListonPaperController {
    private static final Logger logger = LoggerFactory.getLogger(ListonPaperController.class);

    @Autowired
    private ListonPaperService listonPaperService;

    @GetMapping("/lp/maker")
    public String index(){
        return "views/lpaper/paper_maker";
    }

    @RequestMapping("/lp/ga")
    @ResponseBody
    public ListonResult getAllPaper(){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        List<Paper> ret = null;
        try {
            ret = listonPaperService.getAllPaper();
        }
        catch (ListonPaperException ex){
            logger.error(ex.getErrorMsg());
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
        }
        return new ListonResult(code, msg, ret);
    }
}
