package com.liston.exam.liston.chapter;

public class ListonChapter {
    private int id;
    private String chapterName;
    private int chapterSubjectId;
    private String csName;

    public String getCsName() {
        return csName;
    }

    public void setCsName(String csName) {
        this.csName = csName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public int getChapterSubjectId() {
        return chapterSubjectId;
    }

    public void setChapterSubjectId(int chapterSubjectId) {
        this.chapterSubjectId = chapterSubjectId;
    }
}
