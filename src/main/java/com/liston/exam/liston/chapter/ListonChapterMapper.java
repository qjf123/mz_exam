package com.liston.exam.liston.chapter;

import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ListonChapterMapper {

    @Insert("insert into liston_chapter (chapterName, chapterSubjectId, csName) values" +
            " (#{listonChapter.chapterName}, #{listonChapter.chapterSubjectId}," +
            " #{listonChapter.csName})")
    void insertListonChapter(@Param("listonChapter")ListonChapter listonChapter);

    @Select("select * from liston_chapter")
    List<ListonChapter> selectAllChapter();

    @Select("select * from liston_chapter where chapterSubjectId = #{chapterSubjectId}")
    List<ListonChapter> selectListonChapterBySubjectId(@Param("chapterSubjectId") int chapterSubjectId);

    @Delete("delete from liston_chapter where id = #{id}")
    void deleteListonChapterById(int id);

    @Update("update liston_chapter set chapterName = #{chapter.chapterName} where id = #{chapter.id}")
    void updateListonChapterNameById(@Param("chapter") ListonChapter chapter);

}
