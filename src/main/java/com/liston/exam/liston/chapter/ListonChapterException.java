package com.liston.exam.liston.chapter;

public class ListonChapterException extends Exception{
    private String errorMsg ;
    private int errorCode ;

    public ListonChapterException(String errorMsg, int errorCode) {
        super();
        this.errorMsg = errorMsg;
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
