package com.liston.exam.liston.chapter;

import com.alibaba.fastjson.JSON;
import com.liston.exam.liston.Constant;
import com.liston.exam.liston.ListonResult;
import com.liston.exam.liston.question.ListonQuestionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class ListonChapterController {
    private static final Logger logger = LoggerFactory.getLogger(ListonChapterController.class);

    @Resource
    private ListonChapterService listonChapterService;

    @GetMapping("/lc/index")
    public String index(){
        return "views/chapter/chapter_list";
    }

    @RequestMapping("/lc/ga")
    @ResponseBody
    public ListonResult getAllChapter(){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        List<ListonChapter> res = null;
        try{
            res = listonChapterService.getAllChapter();
        }
        catch (ListonChapterException ex){
            logger.error(ex.getMessage());
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
        }
        return new ListonResult(code, msg, res);
    }

    @RequestMapping("/lc/gysid")
    @ResponseBody
    public ListonResult getChapterBySubjectId(int subjectId){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        List<ListonChapter> res = null;
        try{
            res = listonChapterService.getChapterListBySubjectId(subjectId);
        }
        catch (ListonChapterException ex){
            logger.error(ex.getMessage());
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
        }
        return new ListonResult(code, msg, res);
    }

    @RequestMapping("/lc/an")
    @ResponseBody
    public ListonResult addNewChapter(@RequestBody ListonChapter listonChapter){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        try{
            listonChapterService.addnewListonChapter(listonChapter);
        }
        catch (ListonChapterException ex){
            logger.error(ex.getMessage());
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
        }
        return new ListonResult(code, msg);
    }

    @RequestMapping("/lc/uname")
    @ResponseBody
    public ListonResult updateChapterName(@RequestBody ListonChapter listonChapter){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        try{
            listonChapterService.updateListonSubjectById(listonChapter);
        }
        catch (ListonChapterException ex){
            logger.error(ex.getMessage());
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
        }
        return new ListonResult(code, msg);
    }

    @RequestMapping("/lc/dyid")
    @ResponseBody
    public ListonResult deleteChapterByid(int id){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        try{
            listonChapterService.deleteListonSubjectById(id);
        }
        catch (ListonChapterException ex){
            logger.error(ex.getMessage());
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
        }
        return new ListonResult(code, msg);
    }


}
