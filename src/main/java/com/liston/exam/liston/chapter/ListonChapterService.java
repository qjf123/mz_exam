package com.liston.exam.liston.chapter;

import com.liston.exam.liston.Constant;
import com.liston.exam.liston.subject.ListonSubjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ListonChapterService {

    private static final Logger logger = LoggerFactory.getLogger(ListonChapterService.class);

    @Resource
    private ListonChapterMapper listonChapterMapper;

    public List<ListonChapter> getAllChapter() throws ListonChapterException {
        List<ListonChapter> ret = null;
        try {
            ret = listonChapterMapper.selectAllChapter();
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonChapterException(Constant.ListonChapterError.SELECT_ALL_ERROR_MSG,
                    Constant.ListonChapterError.SELECT_ALL_ERROR_CODE);
        }
        return  ret;
    }

    public void addnewListonChapter(ListonChapter listonChapter) throws ListonChapterException {
        try{
            listonChapterMapper.insertListonChapter(listonChapter);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonChapterException(Constant.ListonChapterError.INSERT_ERROR_MSG,
                    Constant.ListonChapterError.INSERT_ERROR_CODE);
        }
    }

    public List<ListonChapter> getChapterListBySubjectId(int subjectId) throws ListonChapterException {
        List<ListonChapter> ret = null;
        try {
            ret = listonChapterMapper.selectListonChapterBySubjectId(subjectId);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonChapterException(Constant.ListonChapterError.SELECT_SUBJECTID_ERROR_MSG,
                    Constant.ListonChapterError.SELECT_SUBJECTID_ERROR_CODE);
        }
        return ret;
    }

    public void deleteListonSubjectById(int id) throws ListonChapterException {
        try {
            listonChapterMapper.deleteListonChapterById(id);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonChapterException(Constant.ListonChapterError.DELETE_ID_ERROR_MSG,
                    Constant.ListonChapterError.DELETE_ID_ERROR_CODE);
        }
    }

    public void updateListonSubjectById(ListonChapter listonChapter) throws ListonChapterException {
        try {
            listonChapterMapper.updateListonChapterNameById(listonChapter);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonChapterException(Constant.ListonChapterError.UPDATE_NAME_ERROR_MSG,
                    Constant.ListonChapterError.UPDATE_NAME_ERROR_CODE);
        }
    }
}
