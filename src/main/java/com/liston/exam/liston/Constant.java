package com.liston.exam.liston;

public class Constant {
    public static class ReturnMessageAndCode{
        public static final String SUCCESS_MSG = "success";
        public static final int SUCCESS_CODE = 0;
        public static final String ERROR_MSG = "error";
        public static final int ERROR_code = -1;
        public static final String INFO_NOT_MATCH = "miss_match";
    }

    public static class ListonQuestionError {
        public static final String UNKNOWN_QUESTION_INFO = "Unknown Question Information!";

        public static final int INSERT_ERROR_CODE = -20;
        public static final String INSERT_ERROR_MSG = "Add ListonQuestion SQL Error !";

        public static final int SELECT_CHAPTER_ID_ERROR_CODE = -21;
        public static final String SELECT_CHAPTER_ID_ERROR_MSG = "Select ListonQuestions By ChapterId SQL Error !";

        public static final int SELECT_ALL_ERROR_CODE = -22;
        public static final String SELECT_ALL_ERROR_MSG = "Select ListonQuestions SQL Error !";

        public static final int DELETE_ID_ERROR_CODE = -23;
        public static final String DELETE_ID__ERROR_MSG = "Delete ListonQuestions By Id SQL Error !";

        public static final int SELECT_QUESTION_TYPE_ERROR_CODE = -24;
        public static final String SELECT_QUESTION_TYPE__ERROR_MSG = "Select ListonQuestions Type SQL Error !";
    }

    public static class ListonSubjectError{

        public static final int INSERT_ERROR_CODE = -30;
        public static final String INSERT_ERROR_MSG = "Add ListonSubject SQL Error !";

        public static final int SELECT_ALL_ERROR_CODE = -31;
        public static final String SELECT_ALL_ERROR_MSG = "Select All ListonSubject SQL Error !";

        public static final int SELECT_LIKE_ERROR_CODE = -32;
        public static final String SELECT_LIKE_ERROR_MSG = "Select Like ListonSubject SQL Error !";

        public static final int DELETE_ID_ERROR_CODE = -33;
        public static final String DELETE_ID_ERROR_MSG = "Delete ListonSubject By Id SQL Error !";

        public static final int UPDATE_NAME_ERROR_CODE = -34;
        public static final String UPDATE_NAME_ERROR_MSG = "Update ListonSubject Name By Id SQL Error !";

    }

    public static class ListonChapterError{
        public static final int INSERT_ERROR_CODE = -40;
        public static final String INSERT_ERROR_MSG = "Add ListonChapter SQL Error !";

        public static final int SELECT_ALL_ERROR_CODE = -41;
        public static final String SELECT_ALL_ERROR_MSG = "Select All ListonChapter SQL Error !";

        public static final int DELETE_ID_ERROR_CODE = -42;
        public static final String DELETE_ID_ERROR_MSG = "Delete ListonChapter By Id SQL Error !";

        public static final int UPDATE_NAME_ERROR_CODE = -43;
        public static final String UPDATE_NAME_ERROR_MSG = "Update ListonChapter Name By Id SQL Error !";

        public static final int SELECT_SUBJECTID_ERROR_CODE = -44;
        public static final String SELECT_SUBJECTID_ERROR_MSG = "Select ListonChapter By SubjectId SQL Error !";
    }

    public static class ListonPaperError{
        public static final int SELECT_ALL_ERROR_CODE = -51;
        public static final String SELECT_ALL_ERROR_MSG = "Select All ListonPaper SQL Error !";
    }
}
