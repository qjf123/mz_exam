package com.liston.exam.liston.subject;

public class ListonSubjectException extends Exception {
    private String errorMsg ;
    private int errorCode ;

    public ListonSubjectException(String errorMsg, int errorCode) {
        super();
        this.errorMsg = errorMsg;
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
