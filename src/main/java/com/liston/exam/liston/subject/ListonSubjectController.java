package com.liston.exam.liston.subject;

import com.liston.exam.liston.Constant;
import com.liston.exam.liston.ListonResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class ListonSubjectController {
    private static final Logger logger = LoggerFactory.getLogger(ListonSubjectController.class);
    @Resource
    private ListonSubjectService listonSubjectService;

    @GetMapping("/ls/index")
    public String index(){
        return "views/subject/subject_list";
    }

    @RequestMapping("/ls/an")
    @ResponseBody
    public ListonResult addNewListonSubject(@RequestBody ListonSubject listonSubject){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        try {
            listonSubjectService.addNewSubject(listonSubject);
        }
        catch (ListonSubjectException ex){
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
            logger.error(msg);
        }
        return new ListonResult(code,msg);
    }

    @RequestMapping("/ls/ga")
    @ResponseBody
    public ListonResult getAllListonSubject(){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        List<ListonSubject> res = null;
        try {
            res = listonSubjectService.selectAllSubject();
        }
        catch (ListonSubjectException ex){
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
            logger.error(msg);
        }
        return new ListonResult(code,msg, res);
    }

    @RequestMapping("/ls/dyid")
    @ResponseBody
    public ListonResult deleteListonSubjectById(int id){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        try {
            listonSubjectService.deleteListonSubjectById(id);
        }
        catch (ListonSubjectException ex){
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
            logger.error(msg);
        }
        return new ListonResult(code,msg);
    }


    @RequestMapping("/ls/sbname")
    @ResponseBody
    public ListonResult getListonSubjectLikeName(String likeName){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        List<ListonSubject> ret = null;
        try {
            ret = listonSubjectService.selectListonSubjectsByNameLike(likeName);
        }
        catch (ListonSubjectException ex){
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
            logger.error(msg);
        }
        return new ListonResult(code,msg, ret);
    }

    @RequestMapping("/ls/uname")
    @ResponseBody
    public ListonResult updateListonSubjectName(@RequestBody ListonSubject listonSubject){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        List<ListonSubject> ret = null;
        try {
            listonSubjectService.updateListonSubjectNameById(listonSubject);
        }
        catch (ListonSubjectException ex){
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
            logger.error(msg);
        }
        return new ListonResult(code,msg, ret);
    }


}
