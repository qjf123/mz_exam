package com.liston.exam.liston.subject;

import com.liston.exam.liston.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ListonSubjectService {
    private static final Logger logger = LoggerFactory.getLogger(ListonSubjectService.class);

    @Resource
    private ListonSubjectMapper listonSubjectMapper;

    public void addNewSubject(ListonSubject listonSubject) throws ListonSubjectException {
        try {
            listonSubjectMapper.insertNewListonSubject(listonSubject);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonSubjectException(Constant.ListonSubjectError.INSERT_ERROR_MSG,
                    Constant.ListonSubjectError.INSERT_ERROR_CODE);
        }
    }

    public void deleteListonSubjectById(int id) throws ListonSubjectException {
        try {
            listonSubjectMapper.deleteListonSubjectById(id);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonSubjectException(Constant.ListonSubjectError.DELETE_ID_ERROR_MSG,
                    Constant.ListonSubjectError.DELETE_ID_ERROR_CODE);
        }
    }

    public List<ListonSubject> selectAllSubject() throws ListonSubjectException {
        List<ListonSubject> listonSubjectList = null;
        try {
            listonSubjectList = listonSubjectMapper.selectAllListonSubject();
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonSubjectException(Constant.ListonSubjectError.SELECT_ALL_ERROR_MSG,
                    Constant.ListonSubjectError.SELECT_ALL_ERROR_CODE);
        }
        return listonSubjectList;
    }

    public List<ListonSubject> selectListonSubjectsByNameLike(String likeName) throws ListonSubjectException {
        List<ListonSubject> listonSubjectList = null;
        try {
            listonSubjectList = listonSubjectMapper.selectListonSubjectsByNameLike(likeName);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonSubjectException(Constant.ListonSubjectError.SELECT_LIKE_ERROR_MSG,
                    Constant.ListonSubjectError.SELECT_LIKE_ERROR_CODE);
        }
        return listonSubjectList;
    }

    public void updateListonSubjectNameById(ListonSubject listonSubject) throws ListonSubjectException {
        try {
            listonSubjectMapper.updateListonSubjectById(listonSubject);
            // 同时更新chapter 中 subjectName字段.
            // ATTENTION! 这里有了侵入。
            listonSubjectMapper.updateListonChapterInfoById(listonSubject);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonSubjectException(Constant.ListonSubjectError.UPDATE_NAME_ERROR_MSG,
                    Constant.ListonSubjectError.UPDATE_NAME_ERROR_CODE);
        }

    }
}
