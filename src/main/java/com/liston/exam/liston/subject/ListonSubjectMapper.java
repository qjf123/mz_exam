package com.liston.exam.liston.subject;

import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ListonSubjectMapper {

    @Insert("insert into liston_subject (subjectName) values(#{listonSubject.subjectName}) ")
    void insertNewListonSubject(@Param("listonSubject") ListonSubject listonSubject);

    @Delete("delete from liston_subject where id = #{id}")
    void deleteListonSubjectById(int id);

    @Select("select * from liston_subject")
    List<ListonSubject> selectAllListonSubject();

    @Select("select * from liston_subject where subjectName like " +
            "CONCAT('%',#{likeName},'%') ")
    List<ListonSubject> selectListonSubjectsByNameLike(String likeName);

    @Update("update liston_subject set subjectName = #{listonSubject.subjectName} " +
            "where id = #{listonSubject.id}")
    void updateListonSubjectById(@Param("listonSubject")ListonSubject listonSubject);

    @Update("update liston_chapter set csName = #{listonSubject.subjectName} " +
            "where chapterSubjectId = #{listonSubject.id}")
    void updateListonChapterInfoById(@Param("listonSubject")ListonSubject listonSubject);
}
