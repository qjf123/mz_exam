package com.liston.exam.liston.question;

import com.alibaba.fastjson.JSON;
import com.liston.exam.liston.Constant;
import com.liston.exam.liston.ListonResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class ListonQuestionController {
    private static final Logger logger = LoggerFactory.getLogger(ListonQuestionController.class);

    @Resource
    private ListonQuestionService listonQuestionService;

    @GetMapping("/lq/index")
    public String index(){
        return "views/lq/question_list";
    }

    @RequestMapping("/lq/gotoan")
    public String gotoAddQuestion(){
        return "views/lq/question_add";
    }

    @RequestMapping("/lq/an")
    @ResponseBody
    public ListonResult addNewQuestion(@RequestBody ListonQuestion listonQuestion){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        try{
            listonQuestion.itemList2OptionString();
            listonQuestionService.addNewListonQuestion(listonQuestion);
        }
        catch (ListonQuestionException ex){
            logger.error(ex.getMessage());
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
        }
        return new ListonResult(code, msg, listonQuestion);
    }

    @RequestMapping("/lq/gci")
    @ResponseBody
    public ListonResult selectListonQuestionByChapterId(int questionChapter){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        List<ListonQuestion> ret = null;
        try {
            ret = listonQuestionService.selectListonQuestionsByChapterId(questionChapter);
        }
        catch (ListonQuestionException ex){
            logger.error(ex.getMessage());
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
        }
        return new ListonResult(code, msg, ret);
    }

    @RequestMapping("/lq/gsi")
    @ResponseBody
    public ListonResult selectListonQuestionBySubjectId(int questionSubject){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        List<ListonQuestion> ret = null;
        try {
            ret = listonQuestionService.selectListonQuestionsBySubjectId(questionSubject);
        }
        catch (ListonQuestionException ex){
            logger.error(ex.getMessage());
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
        }
        return new ListonResult(code, msg, ret);
    }

    @RequestMapping("/lq/gat")
    @ResponseBody
    public ListonResult getAllListonQuestionType(){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        List<ListonQuestionType> ret = null;
        try {
            ret = listonQuestionService.getAllListonQuestionType();
        }
        catch (ListonQuestionException ex){
            logger.error(ex.getMessage());
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
        }
        return new ListonResult(code, msg, ret);
    }

    @RequestMapping("/lq/ga")
    @ResponseBody
    public ListonResult getAllListonQuestion(){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        List<ListonQuestion> ret = null;
        try {
            ret = listonQuestionService.selectListonQuestions();
        }
        catch (ListonQuestionException ex ){
            logger.error(ex.getErrorMsg());
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
        }
        return new ListonResult(code, msg, ret);
    }

    @RequestMapping("/lq/dyid")
    @ResponseBody
    public ListonResult deleteListonQuestion(int id){
        int code = Constant.ReturnMessageAndCode.SUCCESS_CODE;
        String msg = Constant.ReturnMessageAndCode.SUCCESS_MSG;
        try {
            listonQuestionService.deleteListonQuestionById(id);
        }
        catch (ListonQuestionException ex ){
            logger.error(ex.getMessage());
            code = ex.getErrorCode();
            msg = ex.getErrorMsg();
        }
        return new ListonResult(code, msg);
    }
}
