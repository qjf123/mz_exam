package com.liston.exam.liston.question;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.liston.exam.liston.Constant;
import com.liston.exam.liston.chapter.ListonChapter;
import com.liston.exam.liston.chapter.ListonChapterException;
import com.liston.exam.liston.chapter.ListonChapterService;
import io.swagger.models.auth.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class ListonQuestionService {
    private static final Logger logger = LoggerFactory.getLogger(ListonQuestionService.class);

    @Resource
    private ListonQuestionMapper listonQuestionMapper;

    @Resource
    private ListonChapterService listonChapterService;

    @Resource
    private ListonQuestionTypeMapper listonQuestionTypeMapper;

    public List<ListonQuestionType> getAllListonQuestionType() throws ListonQuestionException {
        List<ListonQuestionType> ret = null;
        try {
            ret = listonQuestionTypeMapper.selectAllListonQuestionType();
        }
        catch (Exception exception){
            logger.error(exception.getMessage());
            throw new ListonQuestionException(Constant.ListonQuestionError.SELECT_QUESTION_TYPE__ERROR_MSG,
                    Constant.ListonQuestionError.SELECT_QUESTION_TYPE_ERROR_CODE);
        }
        return ret;
    }

    public void deleteListonQuestionById(int id) throws ListonQuestionException {
        try {
            listonQuestionMapper.deleteListonQuestionById(id);
        }
        catch (Exception exception){
            logger.error(exception.getMessage());
            throw new ListonQuestionException(Constant.ListonQuestionError.DELETE_ID__ERROR_MSG,
                    Constant.ListonQuestionError.DELETE_ID_ERROR_CODE);
        }
    }

    public void addNewListonQuestion(ListonQuestion listonQuestion) throws ListonQuestionException {
        try {
            listonQuestionMapper.insertQuestion(listonQuestion);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonQuestionException(Constant.ListonQuestionError.INSERT_ERROR_MSG,
                    Constant.ListonQuestionError.INSERT_ERROR_CODE);
        }
    }

    public List<ListonQuestion> selectListonQuestions() throws ListonQuestionException {
        List<ListonQuestion> oldQuestionList = listonQuestionMapper.selectAllListonQuestion();
        return switchQuestionInfo(oldQuestionList);
    }

    private List<ListonQuestion> switchQuestionInfo(List<ListonQuestion> oldQuestionList) throws ListonQuestionException {
        HashMap<Integer, ListonChapter> listonChapterHashMap = new HashMap<>();
        HashMap<Integer, ListonQuestionType> typeHashMap = new HashMap<>();
        List<ListonChapter> listonChapters = null;
        // 获取所有问题类型
        List<ListonQuestionType> listonQuestionTypes = null;
        try {
            // 获取所有章节。从中提取科目名称等信息
            // 这里应该用缓存。可是这个系统的规模不会太大，也就无所谓了。
            listonChapters = listonChapterService.getAllChapter();
        } catch (ListonChapterException e) {
            e.printStackTrace();
            logger.error(e.getErrorMsg());
            return null;
        }
        for (ListonChapter listonChapter : listonChapters){
            listonChapterHashMap.put(listonChapter.getId(), listonChapter);
        }

        try {
            // 获取所有问题类型
            listonQuestionTypes =
                    listonQuestionTypeMapper.selectAllListonQuestionType();
        }
        catch (Exception ex){
            ex.printStackTrace();
            logger.error(ex.getMessage());
            return null;
        }
        for(ListonQuestionType listonQuestionType: listonQuestionTypes){
            typeHashMap.put(listonQuestionType.getId(), listonQuestionType);
        }
        int len = oldQuestionList.size();
        try {
            for(int i = 0 ; i < len ; i++){
                ListonQuestion listonQuestion = oldQuestionList.get(i);
                int temp = listonQuestion.getQuestionChapter();
                // 设置章节名称
                if (listonChapterHashMap.containsKey(temp)){
                    listonQuestion.setQuestionChapterStr(listonChapterHashMap.get(temp).getChapterName());
                }
                else {
                    logger.warn(Constant.ListonQuestionError.UNKNOWN_QUESTION_INFO);
                }

                // 设置科目名称
                temp = listonQuestion.getQuestionChapter();
                if (listonChapterHashMap.containsKey(temp)){
                    listonQuestion.setQuestionSubjectStr(
                            listonChapterHashMap.get(temp).getCsName());
                }
                else {
                    logger.warn(Constant.ListonQuestionError.UNKNOWN_QUESTION_INFO);
                }

                // 设置问题类型
                temp = listonQuestion.getQuestionType();
                if (typeHashMap.containsKey(temp)){
                    listonQuestion.setQuestionTypeStr(
                            typeHashMap.get(temp).getName());
                }else {
                    logger.warn(Constant.ListonQuestionError.UNKNOWN_QUESTION_INFO);
                }

                // 设置问题答案.
                // 这块代码有很大改进空间
                ArrayList<String> ansList = new ArrayList<>();
                ArrayList<String> optList = new ArrayList<>();

                TreeSet<String> ansSet = new TreeSet<>
                        (Arrays.asList(listonQuestion.getQuestionAns().split("#")));
                if (temp == 1 || temp == 5 ){
                    List<ListonQuestionItem> listonQuestionItems
                            =  JSONArray.parseArray(listonQuestion.getQuestionOption(), ListonQuestionItem.class);
                    for (ListonQuestionItem item : listonQuestionItems){
                        optList.add(item.getAns());
                        if (ansSet.contains(item.getCode())){
                            ansList.add(item.getAns());
                        }
                    }
                    listonQuestion.setQuestionOptionStr(JSON.toJSONString(optList));
                    listonQuestion.setQuestionAnsStr(JSON.toJSONString(ansList));
                }
                else if (temp == 3){
                    optList.add("正确");
                    optList.add("错误");
                    List<ListonQuestionItem> listonQuestionItems
                            =  JSONArray.parseArray(listonQuestion.getQuestionOption(), ListonQuestionItem.class);
                    String rightAnsCode = listonQuestion.getQuestionAns();
                    if (listonQuestionItems.get(0).getCode().equals(rightAnsCode)){
                        String t = listonQuestionItems.get(0).getAns().equals("Right")?"正确":"错误";
                        ansList.add(t);
                    }
                    else {
                        String t = listonQuestionItems.get(1).getAns().equals("Right")?"正确":"错误";
                        ansList.add(t);
                    }
                    listonQuestion.setQuestionOptionStr(JSON.toJSONString(optList));
                    listonQuestion.setQuestionAnsStr(JSON.toJSONString(ansList));
                }
                else {
                    listonQuestion.setQuestionAnsStr(listonQuestion.getQuestionAns());
                }

                oldQuestionList.set(i, listonQuestion);
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
            logger.error(ex.getMessage());
            throw new ListonQuestionException(Constant.ListonQuestionError.SELECT_ALL_ERROR_MSG,
                    Constant.ListonQuestionError.SELECT_ALL_ERROR_CODE);
        }
        return oldQuestionList;
    }

    public List<ListonQuestion> selectListonQuestionsByChapterId(int questionChapter) throws ListonQuestionException {
        List<ListonQuestion> ret = null;
        try {
            List<ListonQuestion> oldQuestionList = listonQuestionMapper.selectListonQuestionByChapter(questionChapter);
            ret = switchQuestionInfo(oldQuestionList);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonQuestionException(Constant.ListonQuestionError.SELECT_CHAPTER_ID_ERROR_MSG,
                    Constant.ListonQuestionError.SELECT_CHAPTER_ID_ERROR_CODE);
        }
        return ret;
    }

    public List<ListonQuestion> selectListonQuestionsBySubjectId(int questionSubject) throws ListonQuestionException {
        List<ListonQuestion> ret = new ArrayList<>();
        try {
            List<ListonChapter> chapterList= listonChapterService.getChapterListBySubjectId(questionSubject);
            for(ListonChapter chapter: chapterList){
                ret.addAll(selectListonQuestionsByChapterId(chapter.getId()));
            }
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            throw new ListonQuestionException(Constant.ListonQuestionError.SELECT_CHAPTER_ID_ERROR_MSG,
                    Constant.ListonQuestionError.SELECT_CHAPTER_ID_ERROR_CODE);
        }
        return ret;
    }


}
