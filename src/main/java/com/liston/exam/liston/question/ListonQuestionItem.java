package com.liston.exam.liston.question;

public class ListonQuestionItem {
    private String ans;
    private String code;

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
