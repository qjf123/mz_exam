package com.liston.exam.liston.question;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ListonQuestionTypeMapper {
    @Select("select * from exam_questiontype")
    List<ListonQuestionType> selectAllListonQuestionType();

    @Delete("delete from exam_questiontype where id = #{id}")
    void deleteListonQuestionTypeById(int id);
}
