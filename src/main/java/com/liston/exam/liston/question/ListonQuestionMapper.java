package com.liston.exam.liston.question;

import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ListonQuestionMapper {

    @Insert("insert into liston_question (questionType, questionChapter, questionDes, " +
            "questionOption, questionAns, questionScore) " +
            "values (#{listonQuestion.questionType}, #{listonQuestion.questionChapter}, " +
            "#{listonQuestion.questionDes}, #{listonQuestion.questionOption}, " +
            "#{listonQuestion.questionAns}, #{listonQuestion.questionScore} )")
    void insertQuestion(@Param("listonQuestion") ListonQuestion listonQuestion);

    @Select("select * from liston_question where questionChapter = #{questionChapter}")
    List<ListonQuestion> selectListonQuestionByChapter(@Param("questionChapter") int questionChapter);

    @Select("select * from liston_question ")
    List<ListonQuestion> selectAllListonQuestion();

    @Delete("delete from liston_question where id = #{id} ")
    void deleteListonQuestionById(int id);

}
