package com.liston.exam.liston.question;

import com.alibaba.fastjson.JSON;

import java.util.List;

public class ListonQuestion {
    private int id;

    private int questionType;
    private String questionTypeStr;

    private double questionScore;

    private int questionChapter;
    private String questionChapterStr;
    private String questionSubjectStr;
    /*
        问题描述
     */
    private String questionDes;

    /*
        问题选项。
        格式如下：
        [
        {
        "答案1":"答案1描述",
        "答案代码":"代码"
        },
        {
        "答案2":"答案2描述",
        "答案代码":"代码"
        }
        ]
     */
    private List<ListonQuestionItem> listonQuestionItemList;

    /*
        如果是选择题，提供给用户的选项
     */
    private String questionOption;
    // 这个是给前台的字符串，为选项的list字符串
    private String questionOptionStr;

    /*
            多选题使用#分割答案代码。
         */
    private String questionAns;
    private String questionAnsStr;

    public void itemList2OptionString(){
        this.questionOption = JSON.toJSONString(this.listonQuestionItemList);
    }


    public String getQuestionOptionStr() {
        return questionOptionStr;
    }

    public void setQuestionOptionStr(String questionOptionStr) {
        this.questionOptionStr = questionOptionStr;
    }


    public double getQuestionScore() {
        return questionScore;
    }

    public void setQuestionScore(double questionScore) {
        this.questionScore = questionScore;
    }

    public String getQuestionChapterStr() {
        return questionChapterStr;
    }

    public void setQuestionChapterStr(String questionChapterStr) {
        this.questionChapterStr = questionChapterStr;
    }

    public String getQuestionSubjectStr() {
        return questionSubjectStr;
    }

    public void setQuestionSubjectStr(String questionSubjectStr) {
        this.questionSubjectStr = questionSubjectStr;
    }

    public String getQuestionAnsStr() {
        return questionAnsStr;
    }

    public void setQuestionAnsStr(String questionAnsStr) {
        this.questionAnsStr = questionAnsStr;
    }

    public String getQuestionTypeStr() {
        return questionTypeStr;
    }

    public void setQuestionTypeStr(String questionTypeStr) {
        this.questionTypeStr = questionTypeStr;
    }

    public String getQuestionOption() {
        return questionOption;
    }

    public void setQuestionOption(String questionOption) {
        this.questionOption = questionOption;
    }

    public List<ListonQuestionItem> getListonQuestionItemList() {
        return listonQuestionItemList;
    }

    public void setListonQuestionItemList(List<ListonQuestionItem> listonQuestionItemList) {
        this.listonQuestionItemList = listonQuestionItemList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuestionType() {
        return questionType;
    }

    public void setQuestionType(int questionType) {
        this.questionType = questionType;
    }

    public int getQuestionChapter() {
        return questionChapter;
    }

    public void setQuestionChapter(int questionChapter) {
        this.questionChapter = questionChapter;
    }

    public String getQuestionDes() {
        return questionDes;
    }

    public void setQuestionDes(String questionDes) {
        this.questionDes = questionDes;
    }

    public String getQuestionAns() {
        return questionAns;
    }

    public void setQuestionAns(String questionAns) {
        this.questionAns = questionAns;
    }
}
