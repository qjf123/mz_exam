package com.liston.exam.liston;

public class ListonResult {
    private int code;
    private String msg;
    private Object res;

    public ListonResult(int code, String msg, Object res) {
        this.code = code;
        this.msg = msg;
        this.res = res;
    }

    public ListonResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
        this.res = null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getRes() {
        return res;
    }

    public void setRes(Object res) {
        this.res = res;
    }
}
