package com.liston.exam.util;

import com.liston.exam.entity.Log;
import com.liston.exam.entity.User;
import com.liston.exam.service.LogService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @description: WebLogAspect
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 20:17
 */
@Aspect
@Component
public class WebLogAspect {

    @Autowired
    private LogService logService;

    /**
     * 表示 以com.mz.auth.web.controller下面的所有的controller里面所有的方法
     * 作为一个切入点
     */
    @Pointcut("execution(public * com.mz.auth.controller..*.*(..))")
    public void pointcut(){}

    /**
     * 前置通知
     */
    @Before("pointcut()")
    public void handleMethodBefore(JoinPoint joinPoint){
        //RequestContextHolder通过这个类可也拿到请求的一些信息
        RequestAttributes requestAttributes = RequestContextHolder
                .getRequestAttributes();
        //拿到请求的对象
        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();

        Log log  =new Log();
        log.setUrl(request.getRequestURL().toString());
        log.setHttp_method(request.getMethod());
        log.setIp(request.getRemoteAddr());
        log.setClass_method(joinPoint.getSignature().getDeclaringTypeName()+"."
                +joinPoint.getSignature().getName());
        User loginUser = CommonUtils.getLoginUser();
        if(loginUser!=null) {
            log.setUserid(loginUser.getId());
        }
        log.setCreateTime(new Date());
        logService.addLog(log);

    }
}
