package com.liston.exam.util;

import com.liston.exam.entity.Question;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ListonUtil {
    public static List<Question> genRandomList(List<Question> list){
        ArrayList<Question> questions = new ArrayList<>();
        Random random = new Random();
        while (true){
            int max = list.size() - 1;
            if (max == 0){
                break;
            }
            int i = random.nextInt(max);
            questions.add(list.get(i));
            list.remove(i);
        }
        questions.add(list.get(0));
        return questions;
    }
}
