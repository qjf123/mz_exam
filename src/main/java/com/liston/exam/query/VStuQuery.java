package com.liston.exam.query;

import lombok.Data;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 16:29
 */
@Data
public class VStuQuery extends BaseQuery {
    /**
     * 学生昵称
     */
    private String nickName;
    /**
     * 试卷名称
     */
    private String name;
}
