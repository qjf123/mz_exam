package com.liston.exam.query;

import lombok.Data;

/**
 * @description: LogQuery
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 22:46
 */
@Data
public class LogQuery extends BaseQuery {

    private String url;
}
