package com.liston.exam.query;

import lombok.Data;

/**
 * @description: ScoreDetailQuery
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 12:44
 */
@Data
public class ScoreDetailQuery  extends BaseQuery{
    /**
     * 试题的编号
     */
    private Long questionId;
    /**
     * 问题编号
     */
    private Long paperId;
}
