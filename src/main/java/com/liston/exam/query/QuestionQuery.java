package com.liston.exam.query;

import lombok.Data;

/**
 * @description: QuestionQuery 用来接收前台传递参数
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/21 22:42
 */
@Data
public class QuestionQuery extends BaseQuery{
    /**
     * 问题的名称
     */
   private String questionTitle;

}
