package com.liston.exam.query;

import lombok.Data;

/**
 * @description: QuestionOptionsQuery 用来接收前台传递参数
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/21 22:42
 */
@Data
public class QuestionOptionsQuery extends BaseQuery{

}
