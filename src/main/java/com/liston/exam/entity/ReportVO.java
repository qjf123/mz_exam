package com.liston.exam.entity;

import lombok.Data;

/**
 * @description: ReportVO
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 17:43
 */
@Data
public class ReportVO {
    /**
     * 学生昵称
     */
    private String nickName;

    /**
     * 学生总分数
     */
    private String totalScore;
}
