package com.liston.exam.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @description: Question 题型实体类
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/26 8:45
 */
@Data
public class Question {

    /**
     * 问题主键id
     */
    private Long id;
    /**
     * 问题题目
     */
    private String questionTitle;
    /**
     * 问题答案
     */
    private String questionAnswer;
    /**
     * 问题类型id
     * 1 选择题 2填空题 3判断题 4简答题  5  7
     */
    private Long q_typeid;
    /**
     * 问题类型对象
     */
    private QuestionType questionType;
    /**
     * 问题的状态 0 正常 1 停用
     */
    private Long status;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
    /**
     * 选择题选项
     */
    private QuestionXztOptions questionXztOptions;
    /**
     * 创建者id
     */
    private Long creatorId;
    /**
     * 创建者
     */
    private User user;
    /**
     * 分数
     */
    private Integer grade;
}
