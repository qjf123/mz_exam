package com.liston.exam.entity;

import lombok.Data;

/**
 * @description: QuestionType
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/26 8:47
 */
@Data
public class QuestionType {
    /**
     *     主键id
     */
    private Long id;
    /**
     * 题的类型的名称
     */
    private String name;
    /**
     * desc 类型的描述
     */
    private String desc;
    /**
     * typeNum 类型的编号
     */
    private String typenum;

}
