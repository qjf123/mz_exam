package com.liston.exam.entity;

import lombok.Data;

/**
 * @description: QuestionXztOptions 问题选择题的选项数据 实体
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/26 8:51
 */
@Data
public class QuestionXztOptions {
    /**
     * 主键id
     */
    private Long id;
    /**
     * 选择题A选项
     */
    private String optionA;
    /**
     * 选择题B选项
     */
    private String optionB;
    /**
     * 选择题C选项
     */
    private String optionC;
    /**
     * 选择题D选项
     */
    private String optionD;
    /**
     * 问题id
     */
    private Long questionId;
}
