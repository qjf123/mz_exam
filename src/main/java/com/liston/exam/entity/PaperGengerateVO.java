package com.liston.exam.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @description: PaperGengerateVO 生成预览对象
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/26 20:57
 */
@Data
public class PaperGengerateVO {
    /**
     * 试卷id
     */
    private Long id;
    /**
     * 试卷名称
     */
    private String name;
    /**
     * starttime 测试开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") //展示页面
    @DateTimeFormat(pattern =  "yyyy-MM-dd HH:mm:ss") //后台接收时间
    private Date startTime;

    /**
     * endTime 测试结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") //展示页面
    @DateTimeFormat(pattern =  "yyyy-MM-dd HH:mm:ss") //后台接收时间
    private Date endTime;

    /**
     * 一个试卷对应很多问题
     */
    private List<Question> questions = new ArrayList<>();

}
