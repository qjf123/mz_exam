package com.liston.exam.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @description:Log
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 20:27
 */
@Data
public class Log {
    /**
     * 主键id
     */
    private Long id;
    /**
     * 请求路径
     */
    private String url;
    /**
     * 请求方式
     */
    private String http_method;
    /**
     * ip地址
     */
    private String ip;
    /**
     * 操作和类名和方法
     */
    private String class_method;
    /**
     * 日志生成时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 操作用户
     */
    private Long userid;
    /**
     * 操作用户对象
     */
    private User user;

}
