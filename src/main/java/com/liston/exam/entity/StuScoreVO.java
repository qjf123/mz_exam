package com.liston.exam.entity;

import lombok.Data;

/**
 * @description: StuScoreVO
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 14:28
 */
@Data
public class StuScoreVO {
    /**
     * 学生id
     */
    private Long stuId;

    /**
     * 试卷id
     */
    private Long paperId;
    /**
     * 学生昵称
     */
    private String nickName;
    /**
     * 试卷名称
     */
    private String name;
    /**
     * 总分数
     */
    private String totalScore;
}
