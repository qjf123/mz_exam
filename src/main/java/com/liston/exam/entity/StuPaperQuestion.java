package com.liston.exam.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 学生试卷问题试题--查看明细
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 15:27
 */
@Data
public class StuPaperQuestion {
    private Long id;//试卷id
    private String name;//试卷名称
    private List<StuQuestionQueryVO> stuQuestionsList = new ArrayList();//问题集合列表
}
