package com.liston.exam.entity;

import lombok.Data;

/**
 * @description: DicTypeData
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/29 15:29
 */
@Data
public class DicTypeData {
    /**
     * 主键信息
     */
    private Long id;
    /**
     * 数据项对应数据
     */
    private String name;
    /**
     * 关联类型表id
     */
    private Long typeid;
}
