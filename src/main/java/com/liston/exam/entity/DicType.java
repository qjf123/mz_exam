package com.liston.exam.entity;

import lombok.Data;

/**
 * @description: DicType 数据字典类型
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/29 15:28
 */
@Data
public class DicType {
    /**
     * 字典类型主键
     */
    private Long id;
    /**
     * 类型编号 level
     */
    private String sn;
    /**
     * 类型名称
     */
    private String info;
}
