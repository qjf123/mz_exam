package com.liston.exam.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @description: Paper试卷实体
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/25 19:54
 */
@Data
public class Paper {
    /**
     * 试卷主键id
     */
    private Long id;
    /**
     * 试卷名称
     */
    private String name;
    /**
     * 试卷状态
     */
    private Integer status;
    /**
     * starttime 测试开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") //展示页面
    @DateTimeFormat(pattern =  "yyyy-MM-dd HH:mm:ss") //后台接收时间
    private Date startTime;

    /**
     * endTime 测试结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") //展示页面
    @DateTimeFormat(pattern =  "yyyy-MM-dd HH:mm:ss") //后台接收时间
    private Date endTime;

    /**
     * createTime 时间创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") //展示页面
    @DateTimeFormat(pattern =  "yyyy-MM-dd HH:mm:ss") //后台接收时间
    private Date createTime;

    /**
     * 试卷等级
     */
    private Long levelid;

    /**
     * 试卷等级名称
     */
    private String levelname;

}
