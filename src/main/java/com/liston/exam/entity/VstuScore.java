package com.liston.exam.entity;

import lombok.Data;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 16:55
 */
@Data
public class VstuScore {
    /**
     * 学生编号
     */
    private Long stuId;
    /**
     * 试卷Id
     */
    private Long paperId;
    /**
     * 学生昵称
     */
    private String nickName;
    /**
     * 试卷名称
     */
    private String name;
    /**
     * 学生分数
     */
    private Integer totalScore;
}
