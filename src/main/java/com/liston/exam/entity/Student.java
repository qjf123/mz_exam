package com.liston.exam.entity;

import lombok.Data;

import java.util.Date;

/**
 * @description: Student
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/27 15:02
 */
@Data
public class Student {
    /**
     * 主键id
     */
    private Long id;
    /**
     * 用户名
     */
    private String username;
    /**
     *  密码
     */
    private String password;
    /**
     *  电话号码
     */
    private String tel;
    /**
     *  邮件
     */
    private String email;
    /**
     *  学号
     */
    private String stuNum;
    /**
     *  创建时间
     */
    private Date createTime;
    /**
     *  昵称
     */
    private String nickName;

    private String stuId;
    private String stuGrade;
    private String stuMajor;
    private String examNumber;
}
