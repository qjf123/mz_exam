package com.liston.exam.entity;

import lombok.Data;

/**
 * @description: ScoreDetail考试记录类
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 9:25
 */
@Data
public class ScoreDetail {

    /**
     * 记录id
     */
    private Long id;
    /**
     * 学生id
     */
    private Long stuId;
    /**
     * 试卷id
     */
    private Long paperId;
    /**
     * 问题id
     */
    private Long questionId;
    /**
     * 问题标题
     */
    private String questionTitle;
    /**
     * 问题类型
     */
    private Long q_typeid;
    /**
     * 学生答案
     */
    private String questionAnswer;
    /**
     * 问题分数
     */
    private String questionScore;
    /**
     * 正确答案
     */
    private String correntAnswer;
    /**
     * 学生分数
     */
    private String correntScore;

    /**
     * 学生对象
     */
    private Student student;
    /**
     * 试卷对象
     */
    private Paper paper;
}
