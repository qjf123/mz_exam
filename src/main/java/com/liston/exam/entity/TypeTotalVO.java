package com.liston.exam.entity;

import lombok.Data;

/**
 * @description: TypeTotalVO
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/27 9:25
 */
@Data
public class TypeTotalVO {
    /**
     * 题型
     */
    private Long q_typeid;
    /**
     * 总数
     */
    private Long totalNum;
}
