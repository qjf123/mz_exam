package com.liston.exam.entity;

import lombok.Data;

/**
 * @description: StuQuestionQueryVO 学生测试每题记录情况
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 15:28
 */
@Data
public class StuQuestionQueryVO {

    /**
     * 问题Id
     */
    private Long questionId;
    /**
     * 问题题目
     */
    private String questionTitle;
    /**
     * 学生答案
     */
    private String stuAnswer;
    /**
     * 正确答案
     */
    private String correntAnswer;
    /**
     * 问题类型id
     */
    private Long q_typeid;
    /**
     * 问题分数
     */
    private Integer grade;
    /**
     * 正确得分  correntScore
     */
    private Integer correntScore;
    /**
     * 选择题选项
     */
    private QuestionXztOptions questionXztOptions;

}
