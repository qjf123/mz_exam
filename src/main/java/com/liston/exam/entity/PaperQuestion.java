package com.liston.exam.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: PaperQuestion
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/26 17:03
 */
@Data
public class PaperQuestion {
    /**
     * 试卷问题的主键id
     */
    private Long id;
    /**
     * 试卷id
     */
    private Long paperId;
    /**
     * 问题id
     */
    private Long questionId;

    /**
     * 接收前台传递问题列表
     */
    List<Question> questionIdsList = new ArrayList();
}
