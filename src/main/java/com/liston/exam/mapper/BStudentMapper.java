package com.liston.exam.mapper;

import com.liston.exam.entity.BStudent;
import com.liston.exam.entity.Student;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BStudentMapper {
    /*
    * 添加学生
    * 学生登录
    * 学生列表
    * */

    @Insert("insert into t_student (username,password, stuMajor, stuGrade, stuId,examNumber)" +
            " values(#{student.username},#{student.password},#{student.stuMajor}, " +
            "#{student.stuGrade},#{student.stuId},#{student.examNumber})")
    void insertStudent(@Param("student") Student student);

    @Select("select * from t_student")
    List<BStudent> selectAllStudent();

    @Select("select * from t_student limit #{offset},#{page}")
    List<BStudent> selectStudentByPage(Long offset, Long page);

    @Select("select count(*) from t_student")
    Long selectStudenCount();

    @Select("select * from t_student where stuId = #{stuId}")
    List<Student> selectStudentByStuId(@Param("stuId") String stuId);
}
