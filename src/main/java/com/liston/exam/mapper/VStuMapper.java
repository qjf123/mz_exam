package com.liston.exam.mapper;

import com.liston.exam.entity.VstuScore;
import com.liston.exam.query.VStuQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 16:57
 */
@Mapper
public interface VStuMapper {

    /**
     * 查询总数据
     * @param vstuQuery
     * @return
     */
    Long queryTotal(VStuQuery vstuQuery);

    /**
     * 分页数据
     * @param vstuQuery
     * @return
     */
    List<VstuScore> queryData(VStuQuery vstuQuery);
}
