package com.liston.exam.mapper;

import com.liston.exam.entity.Student;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @description: StudentMapper
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/27 15:08
 */
@Mapper
public interface StudentMapper {

    /**
     * 注册学生
     * @param student
     */
    @Insert("insert into t_student(username,password,tel,email,stuNum,createTime,nickName)" +
            " values(#{username},#{password},#{tel},#{email},#{stuNum},#{createTime},#{nickName})")
    void regStu(Student student);

    /**
     * 学生登录
     * @param student
     * @return
     */
    @Select("select * from t_student where examNumber =#{examNumber} and password=#{password}")
    Student login(Student student);

    @Select("select * from t_student where id=#{id}")
    Student getById(String id);
}
