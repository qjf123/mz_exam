package com.liston.exam.mapper;

import com.liston.exam.entity.Question;
import com.liston.exam.entity.QuestionXztOptions;
import com.liston.exam.query.QuestionQuery;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @description: QuestionMapper
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/26 8:57
 */
@Mapper
public interface QuestionMapper {
    /**
     * 查询问题的总数
     * @param questionQuery
     * @return
     */
    Long queryTotal(QuestionQuery questionQuery);

    /**
     * 分页查询的数据
     * @param questionQuery
     * @return
     */
    List<Question> queryData(QuestionQuery questionQuery);

    /**
     * 保存问题方法
     * @param question
     */
    @Insert("insert into exam_questionbank(questionTitle,questionAnswer,grade,q_typeid,status,createTime,creatorId)" +
            "values(#{questionTitle},#{questionAnswer},#{grade},#{q_typeid},#{status},#{createTime},#{creatorId})")
    @Options(useGeneratedKeys = true,keyColumn = "id",keyProperty = "id")
    void addQuestion(Question question);

    /**
     * 保存选择题的选择方法
     * @param questionXztOptions
     */
    @Insert("insert into exam_xzt_options(optionA,optionB,optionC,optionD,questionId)" +
            " values(#{optionA},#{optionB},#{optionC},#{optionD},#{questionId})")
    void addXztOptions(QuestionXztOptions questionXztOptions);

    /**
     * 根据问题的id查询问题
     * @param qid
     * @return
     */
    @Select("select  * from exam_questionbank where id=#{qid}")
    @Results(value={
            @Result(column = "id",property = "id",id=true),
            @Result(column = "id",property = "questionXztOptions",
                    one=@One(select="getQuestionOptionsByQid"))
    })
    Question queryQuestionByQid(Long qid);


    /**
     * 根据问题的id查询选择题的选项
     * @param qid
     * @return
     */
    @Select("select * from exam_xzt_options where questionId=#{id}")
    QuestionXztOptions getQuestionOptionsByQid(Long qid);

    /**
     * 根据问题id 更新数据
     * exam_questionbank(questionTitle,questionAnswer,grade,q_typeid,status,createTime,creatorId)
     * @param question
     */
    @Update("update exam_questionbank set questionTitle=#{questionTitle}" +
            ",questionAnswer=#{questionAnswer},grade=#{grade}," +
            "q_typeid=#{q_typeid},creatorId=#{creatorId} where id=#{id} ")
    void updateQuestion(Question question);

    /**
     * 根据问题id 删除选择题选项表数据
     * @param id
     */
    @Delete("delete from exam_xzt_options where questionId=#{id}")
    void deleteXztOptions(Long id);

    /**
     * 根据问题id 删除问题数据
     * @param id
     */
    @Delete("delete from exam_questionbank where id=#{id}")
    void deleteQuestion(Long id);

    /**
     * 查询所有的对应类型的 问题id
     * @param q_typeid
     * @return
     */
    @Select("select id from exam_questionbank where q_typeid=#{q_typeid} ")
    @ResultType(Long.class)
    List<Long> queryQuestionIdByTypeId(long q_typeid);
}
