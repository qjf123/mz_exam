package com.liston.exam.mapper;

import com.liston.exam.entity.Paper;
import com.liston.exam.entity.PaperGengerateVO;
import com.liston.exam.entity.PaperQuestion;
import com.liston.exam.entity.TypeTotalVO;
import com.liston.exam.query.PaperQuery;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/21 9:00
 */
@Mapper
public interface PaperMapper {

    /**
     * 查询总的条数
     * @param paperQuery
     * @return
     */
    Long queryTotal(PaperQuery paperQuery);

    /**
     * 查询当前页的数据
     * @param paperQuery
     * @return
     */
    List<Paper> queryData(PaperQuery paperQuery);

    /**
     * 保存试卷
     * @param paper
     */
    @Insert("insert into exam_paper(name,startTime,endTime,createTime,status,levelid) " +
            "values (#{name},#{startTime},#{endTime},#{createTime},#{status},#{levelid})")
    void savePaper(Paper paper);

    /**
     * 修改试卷
     * @param paper
     */
    @Update("update exam_paper set name=#{name},startTime=#{startTime},endTime=#{endTime},levelid=#{levelid} " +
            "where id=#{id}")
    void editSavePaper(Paper paper);

    /**
     * 删除组题记录
     * @param id
     */
    @Delete("delete from exam_paper_question where paperId=#{id}")
    void deletePaperQuestion(Long id);

    /**
     * 删除试卷
     */
    @Delete("delete from exam_paper where id=#{id}")
    void deletePaper(Long id);

    /**
     * 查询所有的试卷
     * @return
     */
    @Select("select * from exam_paper")
    List<Paper> queryPaper();

    /**
     * 根据试卷id 查询试卷问题记录
     * @param paperId
     * @return
     */
    @Select("select * from exam_paper_question where paperId=#{paperId}")
    List<PaperQuestion> queryQuestionByPaperId(Long paperId);

    /**
     * 批量插入试卷试题表
     *  insert  insert into exam_paper_question(paperId,questionId) values(1,2),(1,3)
     * @param params
     */
    @Insert("<script>insert into exam_paper_question(paperId,questionId) " +
            "values" +
            "<foreach collection='list' item='item' separator=','>" +
            "(#{item.paperId},#{item.questionId})"+
            "</foreach>"+
            "</script>")
    void insertBatchPaperQuestion(List<Map> params);


    /**
     * 预览试卷方法
     * @param paperId
     * @return
     */
    PaperGengerateVO previewPaper(Long paperId);

    /**
     * 查询题型的总数
     * @return
     */
    @Select("select q_typeid,count(*) totalNum\n" +
            "from exam_questionbank\n" +
            "group by q_typeid")
    List<TypeTotalVO> queryTypeTotal();

    /**
     * 查询所有的试卷
     * @return
     */
    @Select("select * from exam_paper where status = 0 and endTime > CURRENT_TIMESTAMP()")
    List<Paper> queryAll();
}
