package com.liston.exam.mapper;

import com.liston.exam.entity.Student;
import com.liston.exam.query.SysStuQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @description: SysStuMapper
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 16:31
 */
@Mapper
public interface SysStuMapper {
    /**
     * 查询总数据
     * @param sysStuQuery
     * @return
     */
    Long queryTotal(SysStuQuery sysStuQuery);

    /**
     * 分页数据
     * @param sysStuQuery
     * @return
     */
    List<Student> queryData(SysStuQuery sysStuQuery);
}
