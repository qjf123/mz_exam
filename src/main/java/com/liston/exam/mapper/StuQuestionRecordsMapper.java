package com.liston.exam.mapper;

import com.liston.exam.entity.ScoreDetail;
import com.liston.exam.entity.StuPaperQuestion;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 15:35
 */
@Mapper
public interface StuQuestionRecordsMapper {
    /**
     * 查询学生的每题记录
     * @param scoreDetail
     * @return
     */
    StuPaperQuestion queryPaperDetail(ScoreDetail scoreDetail);
}
