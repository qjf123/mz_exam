package com.liston.exam.mapper;

import com.liston.exam.entity.ScoreDetail;
import com.liston.exam.entity.StuScoreVO;
import com.liston.exam.query.ScoreDetailQuery;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @description: ScoreDetailMapper
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 9:27
 */
@Mapper
public interface ScoreDetailMapper {

    @Insert("<script>insert into exam_scoredetail(stuId,paperId,questionId," +
                     "questionTitle,q_typeid," +
                     "questionAnswer,questionScore,correntAnswer,correntScore) values" +
            "<foreach collection='list' item='item' separator=','>" +
                "(#{item.stuId},#{item.paperId},#{item.questionId},#{item.questionTitle}," +
                "#{item.q_typeid},#{item.questionAnswer},#{item.questionScore}," +
                "#{item.correntAnswer},#{item.correntScore})"+
            "</foreach>" +
            "</script>")
    void savePaperTestRecord(List<ScoreDetail> scoreDetailList);

    /**
     * 查询考试记录的总数
     * @param scoreDetailQuery
     * @return
     */
    Long queryTotal(ScoreDetailQuery scoreDetailQuery);

    /**
     * 查询考试记录的分页数据
     * @param scoreDetailQuery
     * @return
     */
    List<ScoreDetail> queryData(ScoreDetailQuery scoreDetailQuery);

    /**
     * 老师阅卷操作
     * @param scoreDetail
     */
    @Update("update exam_scoredetail set correntscore=#{correntScore} where id=#{id}")
    void updateJdtScore(ScoreDetail scoreDetail);

    /**
     * 查询学生的成绩
     * @param stuScoreVO
     * @return
     */
    StuScoreVO queryFrontStuScore(StuScoreVO stuScoreVO);

    @Select("select count(*) from exam_scoredetail where stuId = #{stuId} and paperId = #{paperId}"  )
    int checkJoin(String stuId, String paperId);

//    List<StuScoreVO> queryFrontStuScoreList(StuScoreVO stuScoreVO);
}
