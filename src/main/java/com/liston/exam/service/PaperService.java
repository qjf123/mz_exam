package com.liston.exam.service;

import com.liston.exam.entity.Paper;
import com.liston.exam.entity.PaperGengerateVO;
import com.liston.exam.entity.PaperQuestion;
import com.liston.exam.entity.TypeTotalVO;
import com.liston.exam.query.PaperQuery;
import com.liston.exam.util.PageList;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/21 9:01
 */

public interface PaperService {


    /**
     * 分页查询数据
     * @param paperQuery
     * @return
     */
    PageList listPage(PaperQuery paperQuery);


    /**
     * 保存试卷
     * @param paper
     */
    void savePaper(Paper paper);

    /**
     * 修改保存试卷
     * @param paper
     */
    void editSavePaper(Paper paper);

    /**
     * 删除试卷
     * @param id
     */
    void deletePaper(Long id);

    /**
     * 查询所有的试卷
     * @return
     */
    List<Paper> queryPaper();

    /**
     * 根据试卷id查询对应的问题记录
     * @param paperId
     * @return
     */
    List<PaperQuestion> queryQuestionByPaperId(Long paperId);

    /**
     * 手动组卷
     * @param paperQuestion
     */
    void diyPaperQuestion(PaperQuestion paperQuestion);


    /**
     * 预览试卷方法
     * @param paperId
     * @return
     */
    PaperGengerateVO previewPaper(Long paperId);

    /**
     * 查询题类型的总数
     * @return
     */
    List<TypeTotalVO> queryTypeTotal();

    /**
     * 随机组卷
     * @param mp
     */
    void randomPaperQuestion(Map mp);

    /**
     * 查询所有的试卷
     * @return
     */
    List<Paper> queryAll();
}
