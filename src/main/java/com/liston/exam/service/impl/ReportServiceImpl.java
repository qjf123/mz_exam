package com.liston.exam.service.impl;

import com.liston.exam.entity.ReportVO;
import com.liston.exam.mapper.ReportMapper;
import com.liston.exam.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 17:46
 */
@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportMapper reportMapper;

    /**
     * 查询学生的总成绩
     * @return
     */
    @Override
    public List<ReportVO> findStuTotalScore() {
        return reportMapper.findStuTotalScore();
    }
}
