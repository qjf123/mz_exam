package com.liston.exam.service.impl;

import com.liston.exam.entity.ScoreDetail;
import com.liston.exam.entity.StuPaperQuestion;
import com.liston.exam.mapper.StuQuestionRecordsMapper;
import com.liston.exam.service.StuQuestionRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 15:34
 */
@Service
public class StuQuestionRecordsServiceImpl implements StuQuestionRecordsService {

    @Autowired
    private StuQuestionRecordsMapper stuQuestionRecordsMapper;

    /**
     * 查询学生考试每个题细节
     * @param scoreDetail
     * @return
     */
    @Override
    public StuPaperQuestion queryPaperDetail(ScoreDetail scoreDetail) {
        return stuQuestionRecordsMapper.queryPaperDetail(scoreDetail);
    }
}
