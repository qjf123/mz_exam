package com.liston.exam.service.impl;

import com.liston.exam.entity.Student;
import com.liston.exam.mapper.StudentMapper;
import com.liston.exam.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/27 15:07
 */
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentMapper studentMapper;
    @Override
    public void regStu(Student student) {
        studentMapper.regStu(student);
    }

    /**
     * 学生登录
     * @param student
     * @return
     */
    @Override
    public Student login(Student student) {
        return studentMapper.login(student);
    }

    @Override
    public Student getById(String id) {
        return  studentMapper.getById(id);
    }
}
