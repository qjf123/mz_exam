package com.liston.exam.service.impl;

import com.liston.exam.entity.Question;
import com.liston.exam.entity.QuestionXztOptions;
import com.liston.exam.mapper.QuestionMapper;
import com.liston.exam.query.QuestionQuery;
import com.liston.exam.service.QuestionService;
import com.liston.exam.util.CommonUtils;
import com.liston.exam.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @description: QuestionServiceImpl
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/21 9:02
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionMapper questionMapper;

    /**
     * 分页查询数据
     * @param questionQuery
     * @return
     */
    @Override
    public PageList listPage(QuestionQuery questionQuery) {
        PageList pageList = new PageList();
        //查询总的条数
        Long total = questionMapper.queryTotal(questionQuery);
        pageList.setTotal(total);
        //查询每页的数据
        List<Question> questions =  questionMapper.queryData(questionQuery);
        pageList.setRows(questions);
        return pageList;
    }

    /**
     * 保存问题的方法
     * @param question
     */
    @Override
    @Transactional
    public void addQuestion(Question question) {
        //保存问题表
        question.setStatus(0L);
        question.setCreateTime(new Date());
        question.setCreatorId(CommonUtils.getLoginUser().getId());
        //返回主键
        questionMapper.addQuestion(question);

        //如果是选择题 保存选择题选项表
        if(question.getQ_typeid()==1L || question.getQ_typeid()==5L ){
            QuestionXztOptions questionXztOptions = question.getQuestionXztOptions();
            questionXztOptions.setQuestionId(question.getId());
            questionMapper.addXztOptions(questionXztOptions);
        }

    }

    /**
     * 根据id修改问题方法
          不管什么题型 都去删除一下 选择题选择数据  如果是传过来是选择题，在去插入
     *
     * @param question
     */
    @Override
    @Transactional
    public void editQuestion(Question question) {
        question.setCreatorId(CommonUtils.getLoginUser().getId());
        //修改问题
        questionMapper.updateQuestion(question);
        //如果是选择题
        questionMapper.deleteXztOptions(question.getId());
        if(question.getQ_typeid()==1L){
            questionMapper.addXztOptions(question.getQuestionXztOptions());
        }
    }

    /**
     * 根据问题id删除问题
     * @param id
     *  id = 177   -->根据id查询的问题  根据typeid 决定删不删除选项表数据   在删除问题表
     *  id = 177   --》先删除选项表 在删除问题表
     */
    @Override
    @Transactional
    public void deleteQuestion(Long id) {
        //先删除选择题的选项表
        questionMapper.deleteXztOptions(id);
        //在删除问题表
        questionMapper.deleteQuestion(id);
    }

    /**
     * 根据问题id查询问题
     * @param qid
     * @return
     */
    @Override
    public Question queryQuestionByQid(Long qid) {
        return questionMapper.queryQuestionByQid(qid);
    }




}
