package com.liston.exam.service.impl;

import com.liston.exam.entity.ScoreDetail;
import com.liston.exam.entity.User;
import com.liston.exam.mapper.ScoreDetailMapper;
import com.liston.exam.mapper.UserMapper;
import com.liston.exam.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @description: TeacherServiceImpl老师业务层
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/25 17:24
 */
@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ScoreDetailMapper scoreDetailMapper;


    /**
     * 注册老师
     * @param user
     * @return
     */
    @Override
    public Long addTeacher(User user) {
         user.setType(2); //2代表老师
         user.setCreateTime(new Date());
         //密码加密
         user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
         userMapper.addUser(user);
         return user.getId();
    }

    /**
     * 老师阅卷操作
     * @param scoreDetail
     */
    @Override
    public void updateJdtScore(ScoreDetail scoreDetail) {
        scoreDetailMapper.updateJdtScore(scoreDetail);
    }


}
