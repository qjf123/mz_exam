package com.liston.exam.service.impl;

import com.liston.exam.entity.Paper;
import com.liston.exam.entity.PaperGengerateVO;
import com.liston.exam.entity.PaperQuestion;
import com.liston.exam.entity.TypeTotalVO;
import com.liston.exam.mapper.PaperMapper;
import com.liston.exam.mapper.QuestionMapper;
import com.liston.exam.query.PaperQuery;
import com.liston.exam.service.PaperService;
import com.liston.exam.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/21 9:02
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class PaperServiceImpl implements PaperService {

    @Autowired
    private PaperMapper paperMapper;

    @Autowired
    private QuestionMapper questionMapper;

    /**
     * 分页查询数据
     * @param paperQuery
     * @return
     */
    @Override
    public PageList listPage(PaperQuery paperQuery) {
        PageList pageList = new PageList();
        //查询总的条数
        Long total = paperMapper.queryTotal(paperQuery);
        pageList.setTotal(total);
        //查询每页的数据
        List<Paper> papers =  paperMapper.queryData(paperQuery);
        pageList.setRows(papers);
        return pageList;
    }

    /**
     * 保存试卷
     * @param paper
     */
    @Override
    public void savePaper(Paper paper) {
        paper.setStatus(0);//表示试卷已创建
        paper.setCreateTime(new Date());
        paperMapper.savePaper(paper);
    }

    /**
     * 修改保存试卷
     * @param paper
     */
    @Override
    public void editSavePaper(Paper paper) {
        paperMapper.editSavePaper(paper);
    }

    /**
     * 删除试卷
     *     a 删除exam_paper的记录  paperid = 10017
     *     b 删除试卷对应的组记录 exam_paper_question paperid=10017
     * @param id
     */
    @Override
    @Transactional
    public void deletePaper(Long id) {
        //删除试卷的组题记录
        paperMapper.deletePaperQuestion(id);
        //删除试卷
        paperMapper.deletePaper(id);

    }

    /**
     * 查询所有的试卷
     * @return
     */
    @Override
    public List<Paper> queryPaper() {
        return paperMapper.queryPaper();
    }

    /**
     * 根据试卷id 查询试卷问题记录
     * @param paperId
     * @return
     */
    @Override
    public List<PaperQuestion> queryQuestionByPaperId(Long paperId) {
        return paperMapper.queryQuestionByPaperId(paperId);
    }

    /**
     * 手动组卷
     *   先删除试卷对应的问题
     *   在插入试卷对应的问题表
     * @param paperQuestion
     */
    @Override
    @Transactional
    public void diyPaperQuestion(PaperQuestion paperQuestion) {
         //先删除试卷对应的问题
        paperMapper.deletePaperQuestion(paperQuestion.getPaperId());
         //在插入试卷对应的问题表 完成批量插入 [{paperId:1111,questionId:123},{paperId:1111,questionId:124}]
        List<Map> params = paperQuestion.getQuestionIdsList().stream().map(question -> {
            Map mp = new HashMap();
            mp.put("paperId", paperQuestion.getPaperId());
            mp.put("questionId", question.getId());
            return mp;
        }).collect(Collectors.toList());
        paperMapper.insertBatchPaperQuestion(params);

    }

    /**
     * 预览试卷方法
     * @param paperId
     * @return
     */
    @Override
    public PaperGengerateVO previewPaper(Long paperId) {
        return paperMapper.previewPaper(paperId);
    }

    /**
     * 查询题型的总数
     * @return
     */
    @Override
    public List<TypeTotalVO> queryTypeTotal() {
        return paperMapper.queryTypeTotal();
    }

    /**
     * 随机组卷
     *  xztNum 5  --》10(111,112,115,888,789,222,333,444,555,666)
     *  思路：
     *      查询数据库所有选择题的 id 10(111,112,115,888,789,222,333,444,555,666)
     *      从查询出结果里面 随机选择5 id (根据前端传递数量，循环数量，每次从上面选择一个id，剔除id)
     *      在保存到数据
     * @param mp
     */
    @Override
    @Transactional
    public void randomPaperQuestion(Map mp) {
        //一会传递到后台所有的问题id
        List ids = new ArrayList();
        /**
         * 获取参数
         */
       Long paperId =   Long.valueOf((String)mp.get("paperId"));
       Long xztNum =   Long.valueOf((String)mp.get("xztNum"));
       Long tktNum =   Long.valueOf((String)mp.get("tktNum"));
       Long pdtNum =   Long.valueOf((String)mp.get("pdtNum"));
       Long jdtNum =   Long.valueOf((String)mp.get("jdtNum"));
        Long dxtNum =   Long.valueOf((String)mp.get("dxtNum"));
        //查询所有的选择题的id [111,112,115,222,333,444,555,666]
       List xztIds = questionMapper.queryQuestionIdByTypeId(1L);
        for (int i = 0; i < xztNum; i++) {
            //该值介于[0,10)的区间
            Object target =  xztIds.get(new Random().nextInt(xztIds.size())); //888
            ids.add(target);
            xztIds.remove(target);
        }
        //填空题
        List tktIds = questionMapper.queryQuestionIdByTypeId(2L);
        for (int i = 0; i < tktNum; i++) {
            //该值介于[0,10)的区间
            Object target =  tktIds.get(new Random().nextInt(tktIds.size())); //888
            ids.add(target);
            tktIds.remove(target);
        }
        //判断题
        List pdtIds = questionMapper.queryQuestionIdByTypeId(3L);
        for (int i = 0; i < pdtNum; i++) {
            //该值介于[0,10)的区间
            Object target =  pdtIds.get(new Random().nextInt(pdtIds.size())); //888
            ids.add(target);
            pdtIds.remove(target);
        }

        //简答题
        List jdtIds = questionMapper.queryQuestionIdByTypeId(4L);
        for (int i = 0; i < jdtNum; i++) {
            //该值介于[0,10)的区间
            Object target =  jdtIds.get(new Random().nextInt(jdtIds.size())); //888
            ids.add(target);
            jdtIds.remove(target);
        }

        List dxtIds = questionMapper.queryQuestionIdByTypeId(5L);
        for (int i = 0; i < dxtNum; i++) {
            //该值介于[0,10)的区间
            Object target =  dxtIds.get(new Random().nextInt(dxtIds.size())); //888
            ids.add(target);
            dxtIds.remove(target);
        }

        // ids 提交到后台保存试卷对应的问题 exam_paper_question


        //先删除试卷对应的问题
        paperMapper.deletePaperQuestion(paperId);
        //在插入试卷对应的问题表 完成批量插入 [{paperId:1111,questionId:123},{paperId:1111,questionId:124}]
        List<Map> params = (List)ids.stream().map(id -> {
            Map mp1 = new HashMap();
            mp1.put("paperId", paperId);
            mp1.put("questionId", id);
            return mp1;
        }).collect(Collectors.toList());
        paperMapper.insertBatchPaperQuestion(params);



    }

    /**
     * 查询所有的试卷
     * @return
     */
    @Override
    public List<Paper> queryAll() {
        return paperMapper.queryAll();
    }

    public static void main(String[] args) {
        System.out.println(new Random().nextInt(10));
    }


}
