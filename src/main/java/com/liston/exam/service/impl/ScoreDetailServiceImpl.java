package com.liston.exam.service.impl;

import com.liston.exam.entity.ScoreDetail;
import com.liston.exam.entity.StuScoreVO;
import com.liston.exam.mapper.ScoreDetailMapper;
import com.liston.exam.query.ScoreDetailQuery;
import com.liston.exam.service.ScoreDetailService;
import com.liston.exam.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 9:29
 */
@Service
public class ScoreDetailServiceImpl implements ScoreDetailService {

    @Autowired
    private ScoreDetailMapper scoreDetailMapper;


    @Override
    public boolean checkJoin(String stuId, String paperId) {
        return scoreDetailMapper.checkJoin(stuId, paperId) == 0;
    }

    /**
     * 保存测试记录
     * @param scoreDetailList
     */
    @Override
    public void savePaperTestRecord(List<ScoreDetail> scoreDetailList) {
        scoreDetailMapper.savePaperTestRecord(scoreDetailList);
    }

    /**
     * 分页查询考试记录
     * @param scoreDetailQuery
     * @return
     */
    @Override
    public PageList listPage(ScoreDetailQuery scoreDetailQuery) {
        PageList pageList = new PageList();
        //查询总的条数
        Long total = scoreDetailMapper.queryTotal(scoreDetailQuery);
        pageList.setTotal(total);
        //查询每页的数据
        List<ScoreDetail> scoreDetails =  scoreDetailMapper.queryData(scoreDetailQuery);
        pageList.setRows(scoreDetails);
        return pageList;
    }

    /**
     * 查询学生的成绩
     * @param stuScoreVO
     * @return
     */
    @Override
    public StuScoreVO queryFrontStuScore(StuScoreVO stuScoreVO) {
//        List<StuScoreVO> t = scoreDetailMapper.queryFrontStuScoreList(stuScoreVO);
//        return t.get(t.size()-1);
        return scoreDetailMapper.queryFrontStuScore(stuScoreVO);
    }


}
