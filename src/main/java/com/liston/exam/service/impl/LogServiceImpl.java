package com.liston.exam.service.impl;

import com.liston.exam.entity.Log;
import com.liston.exam.mapper.LogMapper;
import com.liston.exam.query.LogQuery;
import com.liston.exam.service.LogService;
import com.liston.exam.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 20:32
 */
@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogMapper logMapper;

    /**
     * 保存日志方法
     * @param log
     */
    @Override
    public void addLog(Log log) {
        logMapper.addLog(log);
    }

    /**
     * 分页查询方法
     * @param logQuery
     * @return
     */
    @Override
    public PageList listPage(LogQuery logQuery) {
        PageList pageList = new PageList();
        //查询总的条数
        Long total = logMapper.queryTotal(logQuery);
        pageList.setTotal(total);
        //查询每页的数据
        List<Log> logs =  logMapper.queryData(logQuery);
        pageList.setRows(logs);
        return pageList;
    }


}
