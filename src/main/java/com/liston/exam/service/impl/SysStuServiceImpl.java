package com.liston.exam.service.impl;

import com.liston.exam.entity.Student;
import com.liston.exam.mapper.BStudentMapper;
import com.liston.exam.mapper.SysStuMapper;
import com.liston.exam.query.SysStuQuery;
import com.liston.exam.service.SysStuService;
import com.liston.exam.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description: SysStuServiceImpl
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 16:30
 */
@Service
public class SysStuServiceImpl implements SysStuService {

    @Autowired
    private SysStuMapper sysStuMapper;

    @Autowired
    private BStudentMapper bStudentMapper;
    @Override
    public PageList listPage(SysStuQuery sysStuQuery) {
        PageList pageList = new PageList();
        //查询总的条数
        Long total = sysStuMapper.queryTotal(sysStuQuery);
        pageList.setTotal(total);
        //查询每页的数据
        List<Student> students =  sysStuMapper.queryData(sysStuQuery);
        pageList.setRows(students);
        return pageList;
    }

    public void addNewStudent(Student student){
        String stuId = student.getStuId();
        if (!stuId.equals("")){
            List<Student> tempStudent = bStudentMapper.selectStudentByStuId(stuId);
            if (tempStudent == null || tempStudent.size() == 0){
                bStudentMapper.insertStudent(student);
            }
            else {
                System.out.println("已经存在的学生ID");
            }
        }
    }
}
