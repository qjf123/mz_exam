package com.liston.exam.service.impl;

import com.liston.exam.entity.VstuScore;
import com.liston.exam.mapper.VStuMapper;
import com.liston.exam.query.VStuQuery;
import com.liston.exam.service.VStuService;
import com.liston.exam.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description: SysStuServiceImpl
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 16:30
 */
@Service
public class VStuServiceImpl implements VStuService {

    @Autowired
    private VStuMapper vstuMapper;
    @Override
    public PageList listPage(VStuQuery vStuQuery) {
        PageList pageList = new PageList();
        //查询总的条数
        Long total = vstuMapper.queryTotal(vStuQuery);
        pageList.setTotal(total);
        //查询每页的数据
        List<VstuScore> vstuScores =  vstuMapper.queryData(vStuQuery);
        pageList.setRows(vstuScores);
        return pageList;
    }
}
