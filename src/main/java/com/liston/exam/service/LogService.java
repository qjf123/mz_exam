package com.liston.exam.service;

import com.liston.exam.entity.Log;
import com.liston.exam.query.LogQuery;
import com.liston.exam.util.PageList;

/**
 * @description: LogService
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 20:31
 */
public interface LogService {
    /**
     * 加入日志
     * @param log
     */
    void addLog(Log log);


    /**
     * 分页查询方法
     * @param logQuery
     * @return
     */
    PageList listPage(LogQuery logQuery);
}
