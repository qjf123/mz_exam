package com.liston.exam.service;

import com.liston.exam.entity.ReportVO;

import java.util.List;

/**
 * @description: ReportService
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 17:45
 */
public interface ReportService {

    /**
     * 查询学生的总成绩
     * @return
     */
    List<ReportVO> findStuTotalScore();
}
