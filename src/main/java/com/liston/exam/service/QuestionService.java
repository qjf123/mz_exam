package com.liston.exam.service;

import com.liston.exam.entity.Question;
import com.liston.exam.query.QuestionQuery;
import com.liston.exam.util.PageList;

/**
 * @description: QuestionService
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/21 9:01
 */

public interface QuestionService {


    /**
     * 分页查询数据
     * @param questionQuery
     * @return
     */
    PageList listPage(QuestionQuery questionQuery);

    /**
     * 保存问题方法
     * @param question
     */
    void addQuestion(Question question);

    /**
     * 根据问题id 查询问题
     * @param qid
     * @return
     */
    Question queryQuestionByQid(Long qid);

    /**
     * 根据问题id修改问题
     * @param question
     */
    void editQuestion(Question question);

    /**
     * 根据问题id删除问题
     * @param id
     */
    void deleteQuestion(Long id);
}
