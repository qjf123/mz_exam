package com.liston.exam.service;

import com.liston.exam.entity.Student;

/**
 * @description: StudentService
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/27 15:07
 */
public interface StudentService {

    /**
     * 注册学生账号
     * @param student
     */
    void regStu(Student student);

    /**
     * 学生登录
     * @param student
     * @return
     */
    Student login(Student student);

    Student getById(String id);
}
