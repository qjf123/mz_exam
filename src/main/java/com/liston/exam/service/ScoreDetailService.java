package com.liston.exam.service;

import com.liston.exam.entity.ScoreDetail;
import com.liston.exam.entity.StuScoreVO;
import com.liston.exam.query.ScoreDetailQuery;
import com.liston.exam.util.PageList;

import java.util.List;

/**
 * @description: ScoreDetailService
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 9:28
 */
public interface ScoreDetailService {

    boolean checkJoin(String stuId, String paperId);

    /**
     * 保存测试记录
     * @param scoreDetailList
     */
    void savePaperTestRecord(List<ScoreDetail> scoreDetailList);

    /**
     * 分页查询考试记录数据
     */
    PageList listPage(ScoreDetailQuery scoreDetailQuery);


    /**
     * 查询学生成绩
     * @param stuScoreVO
     * @return
     */
    StuScoreVO queryFrontStuScore(StuScoreVO stuScoreVO);

//    List<StuScoreVO> queryFrontStuScoreList(StuScoreVO stuScoreVO);
}
