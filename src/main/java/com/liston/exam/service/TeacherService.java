package com.liston.exam.service;

import com.liston.exam.entity.ScoreDetail;
import com.liston.exam.entity.User;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/25 17:24
 */
public interface TeacherService {
    /**
     * 保存老师方法
     * @param user
     * @return
     */
    Long addTeacher(User user);

    /**
     * 老师阅卷操作
     * @param scoreDetail
     */
    void updateJdtScore(ScoreDetail scoreDetail);
}
