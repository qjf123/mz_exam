package com.liston.exam.service;

import com.liston.exam.entity.ScoreDetail;
import com.liston.exam.entity.StuPaperQuestion;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 15:33
 */
public interface StuQuestionRecordsService {
    /**
     * 查询学生考试每个题细节
     * @param scoreDetail
     * @return
     */
    StuPaperQuestion queryPaperDetail(ScoreDetail scoreDetail);
}
