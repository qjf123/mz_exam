package com.liston.exam.service;

import com.liston.exam.entity.Student;
import com.liston.exam.query.SysStuQuery;
import com.liston.exam.util.PageList;

/**
 * @description: SysStuService
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 16:28
 */
public interface SysStuService {

    /**
     * 分页查询数据
     * @param sysStuQuery
     * @return
     */
    PageList listPage(SysStuQuery sysStuQuery);

    public void addNewStudent(Student student);
}
