package com.liston.exam.service;

import com.liston.exam.query.VStuQuery;
import com.liston.exam.util.PageList;

/**
 * @description: SysStuService
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 16:28
 */
public interface VStuService {

    /**
     * 分页查询数据
     * @param vStuQuery
     * @return
     */
    PageList listPage(VStuQuery vStuQuery);
}
