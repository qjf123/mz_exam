package com.liston.exam.controller;

import com.liston.exam.entity.Student;
import com.liston.exam.query.SysStuQuery;
import com.liston.exam.service.StudentService;
import com.liston.exam.service.SysStuService;
import com.liston.exam.util.MzResult;
import com.liston.exam.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @description: SysStuController
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 16:26
 */
@Controller
public class SysStuController {

    @Autowired
    private SysStuService sysStuService;

    @Autowired
    private StudentService studentService;
    /**
     * 跳转学生的列表页
     * @return
     */
    @GetMapping("/student/index")
    public String index(){
        return "views/student/student_list";
    }

    @GetMapping("/student/add")
    public String add(){
        return "views/student/student_add";
    }


    @GetMapping("/student/listpage")
    @ResponseBody
    public PageList listPage(SysStuQuery sysStuQuery){
        return sysStuService.listPage(sysStuQuery);
    }

    @PostMapping("/student/addStudent")
    @ResponseBody
    public MzResult addStudent(@RequestBody Student bStudent){
        try {
            sysStuService.addNewStudent(bStudent);
            return MzResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }

    @PostMapping("/student/getById")
    @ResponseBody
    public Student addStudent(String id){
        return studentService.getById(id);
    }

}
