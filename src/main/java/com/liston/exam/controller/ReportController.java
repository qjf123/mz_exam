package com.liston.exam.controller;

import com.liston.exam.entity.ReportVO;
import com.liston.exam.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @description: ReportController
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 17:36
 */
@Controller
public class ReportController {

    @Autowired
    private ReportService reportService;

    /**
     * 跳转到报表页面
     * @return
     */
    @GetMapping("/report/index")
    public String index(){
        return "views/report/report_list";
    }

    /**
     * 查询学生的总成绩
     * @return
     */
    @PostMapping("/report/getData")
    @ResponseBody
    public List<ReportVO> getData(){
        return reportService.findStuTotalScore();
    }
}
