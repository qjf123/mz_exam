package com.liston.exam.controller.front;

import com.liston.exam.service.PaperService;
import com.liston.exam.service.ScoreDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @description: FrontIndexController
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/27 11:38
 */
@Controller
public class FrontIndexController {

    @Autowired
    private PaperService paperService;

    @Autowired
    private ScoreDetailService scoreDetailService;



    /**
     * 前台首页的方法
     * @param model
     * @return
     */
    @RequestMapping("/front/index")
    public String index(Model model){
        model.addAttribute("papers",paperService.queryAll());
        return "front/frontIndex";
    }

    /**
     * 跳转学生登录页
     * @return
     */
    @RequestMapping("/front/login")
    public String gotoLoginPage(){
        return "loginIndex";
    }

    /**
     * 跳转学生注册页
     * @return
     */
    @RequestMapping("/front/gotoRegPage")
    public String gotoRegPage(){
        return "front/regIndex";
    }

    @RequestMapping("/front/check")
    @ResponseBody
    public String checkJoin(String userId, String paperId){
        System.out.println("aaaaaaaa: "+userId + " " + paperId);
        return scoreDetailService.checkJoin(userId, paperId)+"";
    }



}
