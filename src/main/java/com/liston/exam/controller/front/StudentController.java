package com.liston.exam.controller.front;

import com.liston.exam.entity.ScoreDetail;
import com.liston.exam.entity.StuScoreVO;
import com.liston.exam.entity.Student;
import com.liston.exam.service.PaperService;
import com.liston.exam.service.ScoreDetailService;
import com.liston.exam.service.StuQuestionRecordsService;
import com.liston.exam.service.StudentService;
import com.liston.exam.util.MzResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * @description: StudentController
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/27 15:05
 */
@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private PaperService paperService;

    @Autowired
    private ScoreDetailService scoreDetailService;

    @Autowired
    private StuQuestionRecordsService stuQuestionRecordsService;

    /**
     * 注册学生
     * @param student
     * @return
     */
    @RequestMapping("/stu/regStu")
    @ResponseBody
    public MzResult regStu(Student student){
        try {
            studentService.regStu(student);
            return MzResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }


    @RequestMapping("/stu/login")
    @ResponseBody
    public MzResult login(Student student, HttpSession session){
        try {
            Student stu = studentService.login(student);
             if(stu==null){
                 return MzResult.error("用户名或者密码错误");
             }else{
                 //登录成功把信息存入session
                 session.setAttribute("stuUser",stu);
                 session.setAttribute("stuUserId",stu.getId());
                 return MzResult.ok();
             }

        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }

    /**
     * 退出
     *  logOut
     */
    @RequestMapping("/stu/logOut")
    public String logout(HttpSession session){
        session.removeAttribute("stuUser");
        return "loginIndex";
    }

    /**
     * /stu/queryScorePage 跳转学生的成绩查询页面
     */
    @RequestMapping("/stu/queryScorePage")
    public String queryScorePage(Model model, HttpSession session){
        Student stuUser = (Student)session.getAttribute("stuUser");
        if(stuUser == null){
            return "redirect:/front/login";
        }

        //查询所有的试卷
        model.addAttribute("papers",  paperService.queryAll());

        return "front/queryScoreIndex";
    }

    /**
     * 查询学生的考试成绩
     *   stuId nickName paperId name  totalScore
     */
    @PostMapping("/stu/queryScoreData")
    @ResponseBody
    public StuScoreVO queryScorePage(@RequestBody StuScoreVO stuScoreVO){
       StuScoreVO result =  scoreDetailService.
               queryFrontStuScore(stuScoreVO);
       return result;
    }

    /**
     * 查看学生考试成绩明细
     * examDetailRecords
     */
    @RequestMapping("/stu/examDetailRecords")
    public String queryFrontExamDetailRecords(Model model,Long paperId,Long stuId){
        ScoreDetail scoreDetail = new ScoreDetail();
        scoreDetail.setStuId(stuId);
        scoreDetail.setPaperId(paperId);
        System.out.println(stuQuestionRecordsService.queryPaperDetail(scoreDetail));
        model.addAttribute("stuPaperQuestionVO", stuQuestionRecordsService.queryPaperDetail(scoreDetail));

        return "front/examDetailIndex";
    }



}
