package com.liston.exam.controller.front;

import com.liston.exam.entity.PaperGengerateVO;
import com.liston.exam.entity.Question;
import com.liston.exam.entity.ScoreDetail;
import com.liston.exam.service.PaperService;
import com.liston.exam.service.ScoreDetailService;
import com.liston.exam.util.ListonUtil;
import com.liston.exam.util.MzResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: FrontExamController 前台测试的controller
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/27 17:12
 */
@Controller
public class FrontExamController {

    @Autowired
    private PaperService paperService;

    @Autowired
    private ScoreDetailService scoreDetailService;

    @RequestMapping("/exam/cj")
    @ResponseBody
    public String checkJoin(String userId, String paperId){
        System.out.println("aaaaaaaa: "+userId + " " + paperId);
        return scoreDetailService.checkJoin(userId, paperId)+"";
    }


    /**
     * 前台弹出测试页面
     * @param model
     * @param paperId
     * @return
     */
    @RequestMapping("/exam/popPaper/{paperId}")
    public String popPaper(Model model , @PathVariable("paperId") Long paperId){
        PaperGengerateVO paperGengerateVO = paperService.previewPaper(paperId);
        ArrayList<Question> temp  =
                (ArrayList<Question>) ListonUtil.genRandomList(paperGengerateVO.getQuestions());
        paperGengerateVO.setQuestions(temp);
        model.addAttribute("examPapersVO",paperGengerateVO);
        return "front/examPaper";
    }

    /**
     * 保存测试记录
     * examPaper
     * 前台examPaperArr --  后台 List<ScoreDetail> scoreDetails
     */
    @RequestMapping("/stu/examPaper")
    @ResponseBody
    public MzResult examPaper(@RequestBody List<ScoreDetail> scoreDetails){
        try {
            scoreDetailService.savePaperTestRecord(scoreDetails);
            return MzResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }
}
