package com.liston.exam.controller;

import com.liston.exam.entity.*;
import com.liston.exam.query.PaperQuery;
import com.liston.exam.service.DicService;
import com.liston.exam.service.PaperService;
import com.liston.exam.util.MzResult;
import com.liston.exam.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @description: PaperController 试卷控制器
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/25 19:51
 */
@Controller
public class PaperController {

    @Autowired
    private PaperService paperService;

    @Autowired
    private DicService dicService;

    /**
     * 跳转试卷列表页面
     * @return
     */
    @GetMapping("/paper/index")
    public String index(Model model){
        //查询试卷等级
        List<DicTypeData> levels = dicService.findLevels();
        model.addAttribute("levels",levels);
        return "views/paper/paper_list";
    }

    /**
     * 跳转 试卷组题页面
     * @return
     */
    @GetMapping("/paper/appendQuestion")
    public String appendQuestion(){
        return "views/paper/paper_question";
    }

    /**
     * 分页查询数据
     * @param paperQuery
     * @return
     */
    @GetMapping("/paper/listpage")
    @ResponseBody
    public PageList listPage(PaperQuery paperQuery){
        return paperService.listPage(paperQuery);
    }

    /**
     * 保存试卷
     * @param paper
     * @return
     */
    @PostMapping("/paper/savePaper")
    @ResponseBody
    public MzResult savePaper(Paper paper){
        try {
             paperService.savePaper(paper);
            return MzResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }

    /**
     * 修改保存试卷
     * @param paper
     * @return
     */
    @PostMapping("/paper/editSavePaper")
    @ResponseBody
    public MzResult editSavePaper(Paper paper){
        try {
            paperService.editSavePaper(paper);
            return MzResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }

    /**
     * 删除试卷
     * @param id
     * @return
     */
    @PostMapping("/paper/deletePaper")
    @ResponseBody
    public MzResult deletePaper(Long id) {
        try {
            paperService.deletePaper(id);
            return MzResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }

    /**
     * 查询所有的试卷
     * @return
     */
    @PostMapping("/paper/queryPaper")
    @ResponseBody
    public List<Paper> queryPaper() {
       return paperService.queryPaper();
    }

    /**
     * 根据试卷id查询所有的问题
     * @return
     */
    @PostMapping("/paper/queryQuestionByPaperId")
    @ResponseBody
    public List<PaperQuestion> queryQuestionByPaperId(@RequestBody PaperQuestion paperQuestion) {
        return paperService.queryQuestionByPaperId(paperQuestion.getPaperId());
    }

    /**
     * 手动组卷
     * @param paperQuestion
     * @return
     */
    @PostMapping("/paper/diyPaperQuestion")
    @ResponseBody
    public MzResult diyPaperQuestion(@RequestBody PaperQuestion paperQuestion) {
        try {
            paperService.diyPaperQuestion(paperQuestion);
            return MzResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }

    /**
     * 根据id查询预览试卷信息 跳转页面进行预览试卷
     * @param paperId
     * @return
     */
    @RequestMapping("/paper/previewPaper/{paperId}")
    public String previewPaper(@PathVariable("paperId") Long paperId, Model model){
        //根据试卷id 查询试卷的信息 跳转页面 进行试卷预览
        PaperGengerateVO paperGengerateVO= paperService.previewPaper(paperId);
        model.addAttribute("paperGengerateVO",paperGengerateVO);
        return "views/paper/paper_preview";
    }

    /**
     * 查询题型的总数
     * queryTypeTotal
     */
    @PostMapping("/paper/queryTypeTotal")
    @ResponseBody
    public List<TypeTotalVO> queryTypeTotal(){
        return paperService.queryTypeTotal();
    }


    /**
     * randomPaperQuestion
     * 随机组卷的方法
     */
    @PostMapping("/paper/randomPaperQuestion")
    @ResponseBody
    public MzResult randomPaperQuestion(@RequestBody Map mp){
        try {
            paperService.randomPaperQuestion(mp);
            return MzResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }


}
