package com.liston.exam.controller;

import com.liston.exam.query.VStuQuery;
import com.liston.exam.service.VStuService;
import com.liston.exam.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @description: VStuController
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 16:26
 */
@Controller
public class VStuController {

    @Autowired
    private VStuService vStuService;
    /**
     * 跳转学生的列表页
     * @return
     */
    @GetMapping("/score/index")
    public String index(){
        return "views/score/score_list";
    }


    @GetMapping("/score/listpage")
    @ResponseBody
    public PageList listPage(VStuQuery vStuQuery){

        return vStuService.listPage(vStuQuery);
    }


}
