package com.liston.exam.controller;

import com.liston.exam.entity.ScoreDetail;
import com.liston.exam.entity.User;
import com.liston.exam.query.ScoreDetailQuery;
import com.liston.exam.query.UserQuery;
import com.liston.exam.service.ScoreDetailService;
import com.liston.exam.service.TeacherService;
import com.liston.exam.service.UserService;
import com.liston.exam.util.MzResult;
import com.liston.exam.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @description: TeacherController 老师控制器
 * @author:soulcoder Mz项目分享圈
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/25 17:22
 */
@Controller
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private UserService userService;

    @Autowired
    private ScoreDetailService scoreDetailService;

    @GetMapping("/teacher/index")
    public String index(){
        return "views/teacher/teacher_list";
    }

    /**
     * 跳转老师阅卷页面
     * @return
     */
    @GetMapping("/teacher/paperExamRecord")
    public String paperExamRecord(){
        return "views/teacher/paper_check";
    }

    /**
     * 查询考试记录的分页方法
     * /teacher/paperExamlistpage
     * @param scoreDetailQuery
     * @return
     */
    @GetMapping("/teacher/paperExamlistpage")
    @ResponseBody
    public PageList listPaperExamPage(ScoreDetailQuery scoreDetailQuery){
        return scoreDetailService.listPage(scoreDetailQuery);
    }

    ///teacher/listpage
    @GetMapping("/teacher/listpage")
    @ResponseBody
    public PageList listPage(UserQuery userQuery){
        userQuery.setType(2);
        return userService.listPage(userQuery);
    }
    /**
     * 注册老师
     * @param user
     * @return
     */
    @PostMapping("/teacher/regTeacher")
    @ResponseBody
    public MzResult regTeacher(User user){
        try {
            Long userid = teacherService.addTeacher(user);
            return MzResult.ok().put("userid",userid);
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }

    /**
     * 老师阅卷操作
     * @param scoreDetail
     * @return
     */
    @PostMapping("/teacher/updateJdtScore")
    @ResponseBody
    public MzResult updateJdtScore(ScoreDetail scoreDetail){
        try {
             teacherService.updateJdtScore(scoreDetail);
            return MzResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }




}
