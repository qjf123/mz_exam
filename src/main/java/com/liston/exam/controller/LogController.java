package com.liston.exam.controller;

import com.liston.exam.query.LogQuery;
import com.liston.exam.service.LogService;
import com.liston.exam.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @description:
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/28 22:44
 */
@Controller
public class LogController {

    @Autowired
    private LogService logService;


    /**
     * 跳转日志的列表页
     * @return
     */
    @GetMapping("/log/index")
    public String index(){
        return "views/log/log_list";
    }

    /**
     * 分页
     * @param logQuery
     * @return
     */
    @GetMapping("/log/listpage")
    @ResponseBody
    public PageList listPage(LogQuery logQuery){

        return logService.listPage(logQuery);
    }
}
