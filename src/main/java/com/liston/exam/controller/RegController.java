package com.liston.exam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @description: RegController
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/25 16:29
 */
@Controller
public class RegController {
    /**
     * 跳转到注册页面
     */
    @RequestMapping("/gotoReg")
    public String gotoReg(){
        return "views/reg";
    }
}
