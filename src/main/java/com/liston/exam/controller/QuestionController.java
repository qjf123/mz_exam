package com.liston.exam.controller;

import com.liston.exam.entity.Question;
import com.liston.exam.query.QuestionQuery;
import com.liston.exam.service.QuestionService;
import com.liston.exam.util.MzResult;
import com.liston.exam.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @description: QuestionController
 * @author:soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/8/26 8:38
 */
@Controller
public class QuestionController {


    @Autowired
    private QuestionService questionService;

    /**
     * 跳转题库列表页
     * @return
     */
    @RequestMapping("/question/index")
    public String index(){
        return "views/question/question_list";
    }


    /**
     * 跳转 新增问题页面
     * @return
     */
    @RequestMapping("/question/gotoAddQuestion")
    public String gotoAddQuestion(){
        return "views/question/question_add";
    }


    /**
     * 跳转题库修改页面
     * /question/gotoEditQuestion/172
     * @return
     */
    @RequestMapping("/question/gotoEditQuestion/{id}")
    public String index(@PathVariable("id") Long id, Model model){
        model.addAttribute("qid",id);
        return "views/question/question_edit";
    }

    /**
     * 根据问题id查询的问题 前台做回显操作
     * @param qid
     * @return
     */
    @PostMapping("/question/queryQuestionByQid")
    @ResponseBody
    public Question queryQuestionByQid( Long qid){
       return questionService.queryQuestionByQid(qid);
    }




    /**
     * 分页查询数据
     * @param questionQuery
     * @return
     */
    @GetMapping("/question/listpage")
    @ResponseBody
    public PageList listPage(QuestionQuery questionQuery){
        return questionService.listPage(questionQuery);
    }

    /**
     * 保存问题方法
     * @param question
     * @return
     */
    @PostMapping("/question/addQuestion")
    @ResponseBody
    public MzResult addQuestion(@RequestBody Question question){
        try {
            questionService.addQuestion(question);
            return MzResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }

    /**
     * 修改保存问题方法
     * @param question
     * @return
     */
    @PostMapping("/question/editQuestion")
    @ResponseBody
    public MzResult editQuestion(@RequestBody Question question){
        try {
            questionService.editQuestion(question);
            return MzResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }

    @GetMapping("/question/deleteQuestion")
    @ResponseBody
    public MzResult deleteQuestion(Long id){
        try {
            questionService.deleteQuestion(id);
            return MzResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return MzResult.error(e.getMessage());
        }
    }

}
